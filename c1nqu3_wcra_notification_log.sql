-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 12, 2020 at 07:57 AM
-- Server version: 5.6.16-1~exp1
-- PHP Version: 7.0.33-0+deb9u6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cinqueterreriviera2019`
--

-- --------------------------------------------------------

--
-- Table structure for table `c1nqu3_wcra_notification_log`
--

CREATE TABLE `c1nqu3_wcra_notification_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `notification` text,
  `date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `c1nqu3_wcra_notification_log`
--

INSERT INTO `c1nqu3_wcra_notification_log` (`id`, `notification`, `date_time`) VALUES
(134, '<strong>1</strong> endpoint url has been requested by client - <strong>http://cinqueterreriviera.info/wp-json/wcra/v1/updateProfileImage/?secret_key=Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W</strong>', '2020-01-06 05:24:03'),
(135, '<strong>22</strong> Recent Activity Log has been deleted by system', '2020-01-06 05:24:03'),
(136, '<strong>1</strong> endpoint url has been requested by client - <strong>http://cinqueterreriviera.info/wp-json/wcra/v1/updateProfileImage/?secret_key=Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W</strong>', '2020-01-06 06:13:02'),
(137, '<strong>1</strong> endpoint url has been requested by client - <strong>http://cinqueterreriviera.info/wp-json/wcra/v1/updateProfileImage/?secret_key=Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W</strong>', '2020-01-06 06:14:33'),
(138, '<strong>1</strong> endpoint url has been requested by client - <strong>http://cinqueterreriviera.info/wp-json/wcra/v1/updateProfileImage/?secret_key=Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W</strong>', '2020-01-06 06:15:06'),
(139, '<strong>1</strong> endpoint url has been requested by client - <strong>http://cinqueterreriviera.info/wp-json/wcra/v1/updateProfileImage/?secret_key=Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W</strong>', '2020-01-06 06:15:11'),
(140, '<strong>1</strong> endpoint url has been requested by client - <strong>http://cinqueterreriviera.info/wp-json/wcra/v1/updateProfileImage/?secret_key=Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W</strong>', '2020-01-06 06:38:57'),
(141, '<strong>1</strong> endpoint url has been requested by client - <strong>http://cinqueterreriviera.info/wp-json/wcra/v1/updateProfileImage/?secret_key=Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W</strong>', '2020-01-06 06:39:22'),
(142, '<strong>1</strong> endpoint url has been requested by client - <strong>http://cinqueterreriviera.info/wp-json/wcra/v1/updateProfileImage/?secret_key=Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W</strong>', '2020-01-06 06:39:31'),
(143, '<strong>1</strong> endpoint url has been requested by client - <strong>http://cinqueterreriviera.info/wp-json/wcra/v1/updateProfileImage/?secret_key=Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W</strong>', '2020-01-06 06:40:12'),
(144, '<strong>1</strong> endpoint url has been requested by client - <strong>http://cinqueterreriviera.info/wp-json/wcra/v1/updateProfileImage/?secret_key=Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W</strong>', '2020-01-06 06:50:39'),
(145, '<strong>1</strong> endpoint url has been requested by client - <strong>http://cinqueterreriviera.info/wp-json/wcra/v1/homePageSearch/?secret_key=Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W</strong>', '2020-01-06 07:54:48'),
(146, '<strong>1</strong> endpoint url has been requested by client - <strong>http://cinqueterreriviera.info/wp-json/wcra/v1/homePageSearch/?secret_key=Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W</strong>', '2020-01-06 09:17:29'),
(147, '<strong>1</strong> endpoint url has been requested by client - <strong>http://cinqueterreriviera.info/wp-json/wcra/v1/homePageSearch/?secret_key=Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W</strong>', '2020-01-06 09:18:07'),
(148, '<strong>1</strong> endpoint url has been requested by client - <strong>http://cinqueterreriviera.info/wp-json/wcra/v1/homePageSearch/?secret_key=Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W</strong>', '2020-01-06 09:55:11'),
(149, '<strong>1</strong> endpoint url has been requested by client - <strong>http://cinqueterreriviera.info/wp-json/wcra/v1/homePageSearch/?secret_key=Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W</strong>', '2020-01-06 09:59:18'),
(150, '<strong>1</strong> endpoint url has been requested by client - <strong>http://cinqueterreriviera.info/wp-json/wcra/v1/homePageSearch/?secret_key=Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W</strong>', '2020-01-06 10:00:23'),
(151, '<strong>1</strong> endpoint url has been requested by client - <strong>http://cinqueterreriviera.info/wp-json/wcra/v1/homePageSearch/?secret_key=Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W</strong>', '2020-01-06 10:01:13'),
(152, '<strong>1</strong> endpoint url has been requested by client - <strong>http://cinqueterreriviera.info/wp-json/wcra/v1/homePageSearch/?secret_key=Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W</strong>', '2020-01-06 10:06:10'),
(153, '<strong>1</strong> endpoint url has been requested by client - <strong>http://cinqueterreriviera.info/wp-json/wcra/v1/homePageSearch/?secret_key=Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W</strong>', '2020-01-06 10:06:42'),
(154, '<strong>1</strong> endpoint url has been requested by client - <strong>http://cinqueterreriviera.info/wp-json/wcra/v1/locationProperties/?secret_key=Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W</strong>', '2020-01-08 04:57:29'),
(155, '<strong>6</strong> Recent Activity Log has been deleted by system', '2020-01-08 04:57:29');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `c1nqu3_wcra_notification_log`
--
ALTER TABLE `c1nqu3_wcra_notification_log`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `c1nqu3_wcra_notification_log`
--
ALTER TABLE `c1nqu3_wcra_notification_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=156;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
