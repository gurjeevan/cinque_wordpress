-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 12, 2020 at 07:56 AM
-- Server version: 5.6.16-1~exp1
-- PHP Version: 7.0.33-0+deb9u6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cinqueterreriviera2019`
--

-- --------------------------------------------------------

--
-- Table structure for table `c1nqu3_wcra_api_endpoints`
--

CREATE TABLE `c1nqu3_wcra_api_endpoints` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `base` text NOT NULL,
  `basedata` text NOT NULL,
  `param` text,
  `secret` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `c1nqu3_wcra_api_endpoints`
--

INSERT INTO `c1nqu3_wcra_api_endpoints` (`id`, `base`, `basedata`, `param`, `secret`) VALUES
(1, 'wcra_test', 'a:1:{s:8:\"callback\";s:23:\"wcra_wcra_test_callback\";}', '', 'Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W'),
(2, 'userLogin', 'a:1:{s:8:\"callback\";s:23:\"wcra_userLogin_callback\";}', 'a:0:{}', 'Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W'),
(3, 'registerUser', 'a:1:{s:8:\"callback\";s:26:\"wcra_registerUser_callback\";}', 'a:0:{}', 'Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W'),
(7, 'homePage', 'a:1:{s:8:\"callback\";s:22:\"wcra_homePage_callback\";}', 'a:0:{}', 'Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W'),
(9, 'locationProperties', 'a:1:{s:8:\"callback\";s:32:\"wcra_locationProperties_callback\";}', 'a:0:{}', 'Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W'),
(10, 'whatToDoDetail', 'a:1:{s:8:\"callback\";s:28:\"wcra_whatToDoDetail_callback\";}', 'a:0:{}', 'Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W'),
(11, 'howToReachDetail', 'a:1:{s:8:\"callback\";s:30:\"wcra_howToReachDetail_callback\";}', 'a:0:{}', 'Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W'),
(12, 'blogDetail', 'a:1:{s:8:\"callback\";s:24:\"wcra_blogDetail_callback\";}', 'a:0:{}', 'Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W'),
(13, 'contactUs', 'a:1:{s:8:\"callback\";s:23:\"wcra_contactUs_callback\";}', 'a:0:{}', 'Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W'),
(18, 'serviceMenu', 'a:1:{s:8:\"callback\";s:25:\"wcra_serviceMenu_callback\";}', 'a:0:{}', 'Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W'),
(19, 'propertyDetail', 'a:1:{s:8:\"callback\";s:28:\"wcra_propertyDetail_callback\";}', 'a:0:{}', 'Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W'),
(20, 'newsletterSubscription', 'a:1:{s:8:\"callback\";s:36:\"wcra_newsletterSubscription_callback\";}', 'a:0:{}', 'Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W'),
(21, 'profileDetail', 'a:1:{s:8:\"callback\";s:27:\"wcra_profileDetail_callback\";}', 'a:0:{}', 'Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W'),
(22, 'testapi', 'a:1:{s:8:\"callback\";s:21:\"wcra_testapi_callback\";}', 'a:0:{}', 'Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W'),
(23, 'faq', 'a:1:{s:8:\"callback\";s:17:\"wcra_faq_callback\";}', 'a:0:{}', 'Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W'),
(24, 'blogList', 'a:1:{s:8:\"callback\";s:22:\"wcra_blogList_callback\";}', 'a:0:{}', 'Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W'),
(25, 'editProfile', 'a:1:{s:8:\"callback\";s:25:\"wcra_editProfile_callback\";}', 'a:0:{}', 'Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W'),
(26, 'userFaq', 'a:1:{s:8:\"callback\";s:21:\"wcra_userFaq_callback\";}', 'a:0:{}', 'Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W'),
(27, 'updateProfileImage', 'a:1:{s:8:\"callback\";s:32:\"wcra_updateProfileImage_callback\";}', 'a:0:{}', 'Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W'),
(28, 'userBookings', 'a:1:{s:8:\"callback\";s:26:\"wcra_userBookings_callback\";}', 'a:0:{}', 'Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W'),
(29, 'searchDealNow', 'a:1:{s:8:\"callback\";s:27:\"wcra_searchDealNow_callback\";}', 'a:0:{}', 'Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W'),
(30, 'homePageSearch', 'a:1:{s:8:\"callback\";s:28:\"wcra_homePageSearch_callback\";}', 'a:0:{}', 'Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W'),
(31, 'bookingPayment', 'a:1:{s:8:\"callback\";s:28:\"wcra_bookingPayment_callback\";}', 'a:0:{}', 'Rhi7suYSARBkvOsIKQmbXEHE4tLxYG7W');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `c1nqu3_wcra_api_endpoints`
--
ALTER TABLE `c1nqu3_wcra_api_endpoints`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `c1nqu3_wcra_api_endpoints`
--
ALTER TABLE `c1nqu3_wcra_api_endpoints`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
