-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 12, 2020 at 07:55 AM
-- Server version: 5.6.16-1~exp1
-- PHP Version: 7.0.33-0+deb9u6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cinqueterreriviera2019`
--

-- --------------------------------------------------------

--
-- Table structure for table `c1nqu3_wcra_api_base`
--

CREATE TABLE `c1nqu3_wcra_api_base` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `Fullname` text NOT NULL,
  `Email` text NOT NULL,
  `ApiSecret` text NOT NULL,
  `Status` int(2) NOT NULL,
  `CreatedAt` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `c1nqu3_wcra_api_base`
--
ALTER TABLE `c1nqu3_wcra_api_base`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `c1nqu3_wcra_api_base`
--
ALTER TABLE `c1nqu3_wcra_api_base`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
