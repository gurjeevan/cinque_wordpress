<?php
defined( 'ABSPATH' ) or die();
//
// Recommended way to include parent theme styles.
//  (Please see http://codex.wordpress.org/Child_Themes#How_to_Create_a_Child_Theme)
//  
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array('parent-style')
    );
}

/*===============User payment==============*/

add_filter("wcra_bookingPayment_callback" , "wcra_bookingPayment_callback_handler");
function wcra_bookingPayment_callback_handler($param)
{
	if(isset($param) && !empty($param))
	{
		

		if(isset($param['card_number']) && $param['card_number'] != '')
		{
			\Stripe\Stripe::setApiKey('pk_test_PzTUV591oIPfE7U0YDoSR3hr00X4dSVnnx');

			$token = \Stripe\Token::create([
			  'card' => [
			    'number' => $param['card_number'],
			    'exp_month' =>$param['card_exp_month'],
			    'exp_year' => $param['card_exp_year'],
			    'cvc' => $param['cvc'],
			  ],
			]);

			$tokenId = $token->id;
		}
		

		$token   		  = $tokenId;
		$price   		  = $param['property_price'];
		$name    		  = $param['property_name'];
		$paymet_type      = $param['payment_type'];
		$booking_price    = $param['booking_price'];
		$client_price     = $param['client_price'];
		$remaining_amount = $param['remaining_amount'];
		$cityTax          = $param['cityTax'];
		$cleaningPrice    = $param['cleaningPrice'];
		$nights           = $param['nights'];
		$price_per_night  = $param['price_per_night'];
		$city_night       = $param['city_night'];
		if ( ! empty ( $paymet_type ) && $paymet_type == 'partial' )
		{
			$price1 = $client_price;
			$remaining_amount = $param['remaining_amount'];
		} 
		else 
		{
			$price1 = $price;
			$remaining_amount = '0';
		}

		$floatVal = floatval($price1);
		if($floatVal && intval($floatVal) != $floatVal)
		{
		    $price1 = $floatVal*100;
		} 
		else 
		{
			$price1 = $price1*100;
		}

		$price1    = str_replace( ".", "", $price1 );

		$sepa_toke = $_POST['sepa_Token'];

		if ( ! empty ( $sepa_toke ) && isset( $sepa_toke ) ) 
		{

			\Stripe\Stripe::setApiKey("sk_test_jazbM8iBDSReNoQ3q33omkmm");

			$customerr  = \Stripe\Customer::create([
			  "email"  => $_POST['sepa_email'],
			  "source" => $sepa_toke,
			]);

			if( ! empty ( $customerr ) ) {

				\Stripe\Stripe::setApiKey("sk_test_jazbM8iBDSReNoQ3q33omkmm");

				$charge = \Stripe\Charge::create([
				  "amount"      => $price1,
				  "currency"    => "EUR",
				  'description' => $name,
				  "customer"    => $customerr,
				  "source"      => $sepa_toke,
				]);

			}

		} 
		elseif ( ! empty( $token ) && isset( $token ) ) 
		{

			\Stripe\Stripe::setApiKey("sk_test_17sQf6WdjDNNx0QKMQulShOI00msW7O5Le");

			$intent = \Stripe\PaymentIntent::create([
			    'payment_method_data' => [
			        'type' => 'card',
			        'card' => ['token' => $token],
			    ],
			    'amount' => $price1,
			    'currency' => 'INR',
			    'confirmation_method' => 'manual',
			    'confirm' => true,
			    'description' => 'Property Name:- '.$name.',
						   Customer Name:- '.$param['p_guestFirstName'].' '.$param['p_guestLastName'].',
						   Customer Email:- '.$param['p_guestEmail'].',
						   Customer Phone No. :- '.$param['p_guestPhoneNumber'].', 
						   No of guest:- '.$param['p_no_of_personn'].', 
						   Dates:- '.$param['p_date_start'].' To '.$param['p_date_end'],
			]);
		}
		if ( ! empty( $intent ) ) 
		{ 
			$current_date = date("Y-m-d");

			$address   = $param['p_guestAddressLine1'].' '.$param['p_guestAddressLine2'];
			$from_date    = $param['p_date_start'];
			$to_date      = $param['p_date_end'];
			$already_paid = $param['property_price'];
			$name         = $param['p_guestFirstName'];
			$sur_name     = $param['p_guestLastName'];
			$email        = $param['p_guestEmail'];
			$phone        = $param['p_guestPhoneNumber'];
			$skype_id     = '';
			$address      = $param['p_guestAddressLine1'].' '.$param['p_guestAddressLine2'];
			$zipcode      = $param['p_guestAddressZip'];
			$country      = $param['p_guestAddressCountry'];
			$nop          = $param['p_no_of_personn'];
			$city         = $param['p_guestAddressCity'];
			$state        = $param['p_guestAddressState'];
			$message      = $param['p_messageToOwner'];
			$new_price    = round( $price, 1 );
		
			$PropertyData = get_option('property_booking_data');

			$date1  = date_create( $param['p_date_start'] );
			$date2  = date_create( $param['p_date_end'] );
			$diff   = date_diff( $date1,$date2 );
			$leaves = $diff->format( "%a" );

			if ( ! empty ( $paymet_type ) && $paymet_type == 'partial' )
			{
				$paid_amount = round( $client_price, 1 );
			}
			else 
			{
				$paid_amount = $new_price;
			}
		 	$array1 = array( 'Status' => 'Success' );

			if ( isset( $array1['Status'] ) && $array1['Status'] == 'Success' ) 
			{
				$dataArray = array(
							'booked_date'      => date("Y-m-d"),
							'days'             => $leaves,
							'property_name'    => $param['property_name'],
							'customer_name'    => $param['p_guestFirstName'].' '.$param['p_guestLastName'],
							'actual_price'     => $price,
							'start_date'       => $param['p_date_start'],
							'end_date'         => $param['p_date_end'],
							'client_price'     => $client_price,
							'paid_amount'      => $paid_amount,
							'remaining_amount' => $remaining_amount,
							'paymet_type'      => $paymet_type,
							'booking_price'    => $booking_price,
							'cityTax'          => $cityTax,
							'cleaningPrice'    => $cleaningPrice,
							'nights'           => $nights,
							'price_per_night'  => $price_per_night,
							'city_night'       => $city_night,
							'already_paid'     => '0',
							'customer_email'   => $param['p_guestEmail'],
							'customer_phone'   => $param['p_guestPhoneNumber'],
							'skype_id'         => '',
							'address'          => $param['p_guestAddressLine1'].' '.$param['p_guestAddressLine2'],
							'zipcode'          => $param['p_guestAddressZip'],
							'country'          => $param['p_guestAddressCountry'],
							'no_of_person'     => $param['p_no_of_personn'],
							'city'             => $param['p_guestAddressCity'],
							'state'            => $param['p_guestAddressState'],
							'message'          => $param['p_messageToOwner'],
							'reservation_ID'   => $reservationID,
						);
				$PropertyData[] = $dataArray;
				
				update_option( 'property_booking_data', $PropertyData );

				$response['success'] = 'true';
				$response['message'] = 'Payment successfully';


			}
			else
			{
				$response['success'] = 'false';
				$response['message'] = 'Error while payment';
			}

		}
		else
		{
			$response['success'] = 'false';
			$response['message'] = 'Error while payment';
		}
	}
	else
	{
		$response['success'] = 'false';
		$response['message'] = 'Parameter missing';
	}

	echo json_encode($response);exit;
}
	

/*===============Home page search api=============*/

add_filter("wcra_homePageSearch_callback" , "wcra_homePageSearch_callback_handler");
function wcra_homePageSearch_callback_handler($param)
{

	if ( $param ) 
	{
		$terms = array();
		$terms = get_terms([
		    'taxonomy' => 'property_type',
		    'hide_empty' => false,
		]);
		$res = array();

        require_once( 'assets/inc/rental_class.php' );
        $RU       = new rentalsUnited(); 
        $lid      = $param['Locationid'];
        $DateFrom = $param['DateFron'];
        $DateTo   = $param['DateTo'];

        // It wasn't there, so regenerate the data and save the transient
        $UserDataTransient = array(
          	'Locationid' => $param['Locationid'],
          	'DateFron'   => $param['DateFron'],
          	'DateTo'     => $param['DateTo'],
          	'adults'     => $param['adults'],
          	'children'   => $param['children'],
        );
        set_transient( 'UserDataTransient', $UserDataTransient, 300 );

        $day_arrrr2 = array();
        $day_arrrr3 = array();
        
        $date1  = date_create( $DateFrom );
        $date2  = date_create( $DateTo );
        $diff   = date_diff( $date1, $date2 );
        $leaves = $diff->format("%a");
        for ( $i=0; $i < $leaves ; $i++ ) 
        { 
            $start_date1 = date( 'Y-m-d', strtotime( $DateFrom . ' +'.$i.' day' ) );
            array_push( $day_arrrr3, $start_date1 );
        }
        array_push( $day_arrrr2, $DateTo );

        $main_arr_holi = array_merge( $day_arrrr2, $day_arrrr3 );

    
        if ( isset( $lid ) && ! empty ( $lid ) ) 
        {


            $count = 1;
            $Adults         = $param['adults'];
            $Children       = $param['children'];
            $total_person   = $Adults + $Children;

            $cache_file_2 = get_stylesheet_directory() . '/cache-files/api-cache-all-data-'.$lid.'.json';

            if ( file_exists( $cache_file_2 ) ) 
            {

                $fetch_data = file_get_contents( $cache_file_2 );
                $fetch_data = json_decode( $fetch_data  );
                $fetch_data = json_encode( $fetch_data );
                $fetch_data = json_decode( $fetch_data, true );

                foreach ( $fetch_data as $key => $value ) 
                {


                    $result = '';
                        
                    if ( ! empty ( $value["BookDates"] ) ) 
                    {
                        $result = array_intersect( $value["BookDates"], $main_arr_holi );
                    }


                    if( empty ( $result ) ) 
                    {

                        $cache_file = get_stylesheet_directory(). '/cache-files/api-cache-'.$value['ID'].'.json';
                        if ( ! file_exists( $cache_file ) ) 
                        {
                            $PropertyDetail = $RU->getProperty( $value['ID'] );
                            $PropertyDetail = $RU->json_cached_api_results_property( $PropertyDetail, $value['ID'] );
                            $json           = json_encode( $PropertyDetail );
                            $Property       = json_decode( $json, true ); 
                        } 
                        else 
                        {
                            $Property = file_get_contents( $cache_file );
                            $Property = json_decode( $Property, true );
                        }
                                                            
                        $currency       = $Property['Property']['@attributes']['Currency'];
                        $CanSleepMax    = $Property['Property']['CanSleepMax'];
                        $StandardGuests = $Property['Property']['StandardGuests'];
                        $CleaningPrice  = $Property['Property']['CleaningPrice'];
                        $request_ids    = array();

                        if ( $total_person < $CanSleepMax || $total_person == $CanSleepMax ) 
                        {

                            $realRates = get_stylesheet_directory(). '/cache-files/api-cache-rates-'.$value['ID'].'.json';
                            if ( ! file_exists( $realRates ) ) 
                            {
                                $price  = $RU->getRealtimeRates( $value['ID'], $DateFrom, $DateTo );
                                $price  = $RU->json_cached_api_realtime( $price, $value['ID'] );
                                $json_1 = json_encode( $price );
                                $rate   = json_decode( $json_1, true );
                            } 
                            else 
                            {
                                $rate = file_get_contents( $realRates );
                                $rate = json_decode( $rate, true );
                            }

                            /* Loop for instant booking properties only */
                            $args = array(
                              'post_type' => 'product',
                              'posts_per_page' => -1,
                            );
                            $properties = new WP_Query($args);
                            if( $properties->have_posts() ) 
                        	{ 
                        		$countt=1;
                        		foreach($properties->posts  as $key=>$val )
                        		{
                        			$each_id = get_post_meta( $val->ID, 'propert_id', true );
                        		
                                    if ( $value['ID'] == $each_id ) 
                                    {

                                        if ( get_post_meta( $val->ID, 'instant_book', true ) ) 
                                        {
                                        	$res[$key]['slug'] = $val->post_name;
								    		$res[$key]['title'] = get_the_title($val->ID);
								    		$res[$key]['rating'] = get_field( 'star_rating', $val->ID );
								    		/*=== Geting status ====*/

								    		if ( get_post_meta( $val->ID, 'instant_book', true ) ) 
								    		{
								    			$res[$key]['status'] = 'Instant Bookable';
								    		}
								    		else
								    		{
								    			$res[$key]['status'] = 'On Request';
								    		}

								    		/*=== Geting price ====*/

								    		$property_id = get_post_meta( $val->ID, 'propert_id', true );

								    		
								    		$cache_file1 = get_stylesheet_directory(). '/cache-files/api-cache-taxo-'.$property_id.'.json';

								            if ( ! file_exists( $cache_file1 ) ) 
								            {
								                $image_response = $RU->getRates( $each_id );
								                $image_response = $RU->json_cached_api_taxo( $image_response, $property_id );
								                $image_json     = json_encode( $image_response );
								                $array11        = json_decode( $image_json, true );
								            } 
								            else 
								            {
								                $json11  = file_get_contents( $cache_file1 );
								                $array11 = json_decode( $json11, true );
								            }

								  
								            $currentdate = date("Y-m-d");
								            $DatesPerDay = array();

								            if ( $array11['Status'] == 'Success' ) 
								            {
								                $season_size = sizeof( $array11["Prices"]["Season"] );
								              	if ( $season_size == '3' ) 
								              	{
									                $data = array(
									                    'date' => $array11["Prices"]["Season"]["@attributes"]["DateFrom"],
									                    'price'=> $array11["Prices"]["Season"]["Price"]
									                  );
									                  array_push( $DatesPerDay, $data );

								                    $data = array(
								                      'date' => $array11["Prices"]["Season"]["@attributes"]["DateTo"],
								                      'price'=> $array11["Prices"]["Season"]["Price"]
								                    );
								                  	array_push( $DatesPerDay, $data );
								              	} 
								              	else 
								              	{
								                    for ( $i=0; $i < $season_size+1; $i++ ) 
								                    {

								                      	if ( ! empty( $array11["Prices"]["Season"][$i]["@attributes"]["DateFrom"] ) ) {
								                          	$data = array(
									                            'date' => $array11["Prices"]["Season"][$i]["@attributes"]["DateFrom"],
									                            'price'=> $array11["Prices"]["Season"][$i]["Price"]
								                          	);
								                          	array_push( $DatesPerDay, $data );
								                      	}

								                      	if ( ! empty( $array11["Prices"]["Season"][$i]["@attributes"]["DateTo"] ) ) {
								                          	$data = array(
									                            'date' => $array11["Prices"]["Season"][$i]["@attributes"]["DateTo"],
									                            'price'=> $array11["Prices"]["Season"][$i]["Price"]
								                          	);
								                          	array_push( $DatesPerDay, $data );
								                      	} 
								                    }
								                }
								            }
								            $start_count = 1;
								           	$start_status = 0;
								            if ( ! empty ( $DatesPerDay ) ) 
								            {
								            	foreach ( $DatesPerDay as  $value ) 
								            	{
								            		
								            		if ( $value['date'] == $currentdate && $start_count == 1 ) 
								            		{
								            			$startprice = intval( $value['price'] );
								            			$start_status++; 
								            			$start_count++; 
								            		}
								            	} 
								            }
								            if ( $start_status == 0 ) 
								            {
									          	$start_price = explode('.', get_field( "starts_from",$val->ID ) );
									          	$startprice = $start_price[0];
									        }

									         $res[$key]['price'] = '€'.' '.$startprice;

									        /*====get icons detail====*/

									        if ( ! empty ( $property_id ) ) 
									        {
												$cache_files = get_stylesheet_directory(). '/cache-files/api-cache-'.$property_id.'.json';
												if ( ! file_exists( $cache_files ) ) 
												{
								                    $image_response = $RU->getProperty( $property_id );
								                    $image_response = $RU->json_cached_api_results_property( $image_response, $property_id );
								                    $image_json    = json_encode( $image_response );
								                    $images_array1   = json_decode( $image_json, true );
								                } 
								                else 
								                {
								                    $fetch_data   = file_get_contents( $cache_files );
								                    $images_array1 = json_decode( $fetch_data, true );
								                }
								            }
								         
											$CompositionRoomAmenities = $images_array1["Property"]['CompositionRoomsAmenities']['CompositionRoomAmenities'];
											$RoomIDS = array();
								            if( ! empty( $CompositionRoomAmenities ) ) {
								                foreach ( $CompositionRoomAmenities as $value ) {
								                    array_push( $RoomIDS, $value["@attributes"]["CompositionRoomID"] );
								                }
								               $NumRoomIDS = array_count_values($RoomIDS);
								            }
									       
									        if( isset( $NumRoomIDS[257] ) && ! empty( $NumRoomIDS[257] ) ) 
								        	{
								        		$rooms =  $NumRoomIDS[257];
								        	}
								        	else
								        	{
								        		$rooms = '-';
								        	}
								        	if( isset( $NumRoomIDS[81] ) && ! empty( $NumRoomIDS[81] ) ) 
								        	{
								                $bathrooms =  $NumRoomIDS[81];
								        	}
								        	else
								        	{
								        		$bathrooms = '-';
								        	}

											$res[$key]['icons'] = array(
								        		'guests'=> (!empty(get_post_meta($val->ID, 'max_guest', true))) ? get_post_meta($val->ID, 'max_guest', true) : '-',
								        		'rooms'=> $rooms,
								        		'bathrooms'=> $bathrooms,
								        	);

								        	/*=======get gallery=====*/

								        	$images1 =  array();
											if ( isset( $images_array1 ) && ! empty ( $images_array1 ) ) 
								        	{
								        		$image_count = 1;
												foreach ( $images_array1['Property']['Images']['Image'] as  $values ) 
								                {
								                    if ( $image_count < 4 ) 
								                    {
								                    	$images1[] = $values;
								                    	$image_count++;
								                    }
								                }
								        	}
								        	$res[$key]['gallery'] =  $images1;
                                         	
              							} 
              						} 
                        		}
                                
                   			} 
                   		} 
					}
					
                }
                if(isset($res) && !empty($res))
				{
					$arr = array_values($res);
				}
				else
				{
					$arr = array();
				}
				
			   	$response['success'] = 'true';
				$response['message'] = 'Success';
				$response['locationProperties'] = $arr;
				$response['propertyType'] = $terms;
			}
            else
            {
            	$response['success'] = 'true';
				$response['message'] = 'Success';
				$response['locationProperties'] = array();
				$response['propertyType'] = $terms;
            }
        }
        else
        {
        	$response['success'] = 'true';
			$response['message'] = 'Success';
			$response['locationProperties'] = array();
			$response['propertyType'] = $terms;
        }
	}
	else
	{
		$response['success'] = 'true';
		$response['message'] = 'Parameter missing';
	}
	echo json_encode($response);exit;
}

/*=================Search Deal Now==============*/

add_filter("wcra_searchDealNow_callback" , "wcra_searchDealNow_callback_handler");
function wcra_searchDealNow_callback_handler($param)
{
	if ( isset ( $param['check_in_date'] ) && isset ( $param['check_out_date'] ) && isset ( $param['no_of_person'] ) && isset ( $param['property_id'] ) && isset ( $param['product_id'] )) 
	{
		$nop   = $param['no_of_per'];
        $pid   = $_POST['product_id'];
        //$ppnt  = $_POST['price_per_nigth'];
        //$layout = $_POST['layout'];
        $n_o_p = $param['no_of_per'] + 1;
        $RU  = new rentalsUnited();

        $property1 = $RU->getProperty( $param['property_id'] );
        $json      = json_encode( $property1 );
        $array     = json_decode( $json, true );
        $propertyName  = $array['Property']['Name'];
        $CleaningPrice = intval( $array['Property']['CleaningPrice'] );

        /* Get booking dates */
        $responses = $RU->getCalendar( $param['property_id'] );

        $json     = json_encode( $responses );
        $getPropertyTypes = json_decode( $json, true );

        
        $day_arrrr  = array();
        $day_arrrr2 = array();
        $day_arrrr3 = array();
		
		function is_multi($a) 
        {
        	if(!empty($a))
        	{
        		$rv = array_filter($a,'is_array');
        	}
        	else
        	{
        		$rv = array();
        	}
            
            if(count($rv)>0) 
        	{
        		return true;
        	}
        	else
        	{
        		return false;
        	}
            
        }
        
        $multi_arra = is_multi($getPropertyTypes['PropertyBlock']['Block']);

        if ( $multi_arra == false ) 
        {
          	$strt_date = $getPropertyTypes['PropertyBlock']['Block']['DateFrom'];
          	$end_date  = $getPropertyTypes['PropertyBlock']['Block']['DateTo'];

          	$date1 = date_create( $end_date );
          	$date2 = date_create( $strt_date );
          	$diff  = date_diff( $date1, $date2 );
          	$leaves = $diff->format("%a");
          	for ( $i=1; $i < $leaves ; $i++ ) 
          	{ 
              	$start_date1 = date( 'Y-m-d', strtotime( $strt_date . ' +'.$i.' day' ) );
              	array_push( $day_arrrr3, $start_date1 );
          	}
          	array_push( $day_arrrr2, $end_date );

          	$main_arr_holi = array_merge( $day_arrrr2, $day_arrrr3 );
            $size = sizeof($main_arr_holi);

        } 
        else 
        {
          	foreach ( $getPropertyTypes['PropertyBlock']['Block'] as $key => $value) {
                $strt_date = $value['DateFrom'];
                $end_date  = $value['DateTo'];
                
                $date1 = date_create( $end_date );
                $date2 = date_create( $strt_date );
                $diff  = date_diff( $date1, $date2 );
                $leaves = $diff->format("%a");
                for ( $i=1; $i < $leaves ; $i++ ) { 
                    $start_date1 = date( 'Y-m-d', strtotime( $strt_date . ' +'.$i.' day' ) );
                    array_push( $day_arrrr3, $start_date1 );
                }
                array_push( $day_arrrr2, $end_date );
            }
            $main_arr_holi = array_merge( $day_arrrr2, $day_arrrr3 );
            $size = sizeof($main_arr_holi);
        }

        /* Requested dates */
        if(!empty($getPropertyTypes['PropertyBlock']['Block']))
        {
        	foreach ( $getPropertyTypes['PropertyBlock']['Block'] as $key => $value) 
	        {
	            $strt_date = $value['DateFrom'];
	            $end_date  = $value['DateTo'];
	            
	            $date1 = date_create( $end_date );
	            $date2 = date_create( $strt_date );
	            $diff  = date_diff( $date1, $date2 );
	            $leaves = $diff->format("%a");
	            for ( $i=1; $i < $leaves ; $i++ ) { 
	                $start_date1 = date( 'Y-m-d', strtotime( $strt_date . ' +'.$i.' day' ) );
	                array_push( $day_arrrr3, $start_date1 );
	            }
	            array_push( $day_arrrr2, $end_date );
	        }
        }
        
        $main_arr_holi = array_merge( $day_arrrr2, $day_arrrr3 );
        $size = sizeof($main_arr_holi);

        $day_arrrr22 = array();
        $day_arrrr33 = array();
        $date11 = date_create( $param['check_in_date'] );
        $date22 = date_create( $param['check_out_date'] );
        $diff1  = date_diff( $date11, $date22 );
        $leaves1 = $diff1->format("%a");
        for ( $i=0; $i < $leaves1 ; $i++ ) 
        { 
            $start_date11 = date( 'Y-m-d', strtotime( $param['check_in_date'] . ' +'.$i.' day' ) );
            array_push( $day_arrrr33, $start_date11 );
        }
        array_push( $day_arrrr22, $param['check_out_date'] );

        $main_arr_holi1 = array_merge( $day_arrrr22, $day_arrrr33 );
        /* End of Requested dates */

        $result = array_intersect( $main_arr_holi,$main_arr_holi1 );

        if ( empty ( $result ) ) 
        {

	        $response_1 = $RU->getRealtimeRates( $param['property_id'], $param['check_in_date'], $param['check_out_date'] );
	        $json_1     = json_encode( $response_1 );
	        $rate       = json_decode( $json_1, true );
	        $Nights     = sizeof( $main_arr_holi1 ) - 1;


	        if ( $rate['Status'] == 'Success' ) 
	        {
	            $price_size = sizeof( $rate['PropertyPrices']['PropertyPrice'] );

	            $getBookingDet = array();

	            for ( $i=0; $i < $price_size; $i++ ) 
	            { 
	              	if ( $nop == $i ) 
	              	{
	                	$price = $rate['PropertyPrices']['PropertyPrice'][$i];
		                if ( ! empty( $CleaningPrice ) ) 
		                {
		                  	$BookigPrice = $price - $CleaningPrice;
		                } 
		                else 
		                {
		                  	$CleaningPrice = get_post_meta( $pid, 'cleaning_fee', true );
		                  	$CityTax       = get_post_meta( $pid, 'city_tax', true );
		                  	$city_night    = $CityTax * $Nights;          
		                  	$BookigPrice   = $price - ( $CleaningPrice + $city_night );
		                }

	                	$client_price = ( 30 / 100 ) * $BookigPrice;
	                	$client_price = round( $client_price, 1 );

	                	$remaining_amount = $BookigPrice - $client_price;

	                	$getBookingDet['check_in_date'] = $param['check_in_date'];
	                	$getBookingDet['check_out_date'] = $param['check_out_date'];
	                	$getBookingDet['total_nights'] = $Nights;
	                	$getBookingDet['price'] = $BookigPrice;
	                	$getBookingDet['cleaning_fee'] = '€'.' '.$CleaningPrice.' Per Stay. To be paid locally in cash.';
	                	$getBookingDet['city_tax'] = '€ 1 Per Person. To be paid locally in cash.';
	                	$getBookingDet['full_payment'] = $BookigPrice;
	                	$getBookingDet['deposit'] = 'Pay 30% deposit (€ '.$client_price.')';
	                	$getBookingDet['min_deposit'] = $client_price;
	                	$getBookingDet['property_price'] = $BookigPrice+$CleaningPrice;
	                	$getBookingDet['cleaning_price'] = $CleaningPrice;
	                	$getBookingDet['remaining_amount'] = $BookigPrice-$client_price;
	                }
	            }
	            $response['success'] = 'true';
				$response['message'] = 'Success';
				$response['details'] = $getBookingDet;
	        }
	        else
	        {
	        	$response['success'] = 'false';
	        	if($rate['Status'] == '')
	        	{
	        		$response['message'] = 'Error';
	        	}
	        	else
	        	{
	        		$response['message'] = $rate['Status'];
	        	}
	        	$response['details'] = new stdClass();
			}
		} 
		else
		{
			$response['success'] = 'false';
			$response['message'] = 'Those dates are not available..!';
			$response['details'] = new stdClass();
		}       
	}
	else
	{
		$response['success'] = 'false';
		$response['message'] = 'All fields are required';
		$response['details'] = new stdClass();
	}
	echo json_encode($response);exit;
}

/*==============Get user for login screen=============*/

add_filter("wcra_userLogin_callback" , "wcra_userLogin_callback_handler");			
function wcra_userLogin_callback_handler($param)
{	
	
	global $wpdb;			
	$email = $param['email'];
	$password = base64_decode($param['password']);
	if($email == '')
	{
		$response['success'] = 'false';
		$response['user_id'] = '';
		$response['message'] = 'Email required';
	}
	elseif($password == '')
	{
		$response['success'] = 'false';
		$response['user_id'] = '';
		$response['message'] = 'Password required';
	}
	else
	{
		$userdata = $wpdb->get_row("SELECT * FROM c1nqu3_users WHERE user_email = '".$email."'");
		if ( $userdata && wp_check_password( $password, $userdata->user_pass, $userdata->ID ) ) 
		{
			$response['success'] = 'true';
			$response['user_id'] = "$userdata->ID";
			$response['message'] = 'Login successfully';
		}
		else
		{
			$response['success'] = 'false';
			$response['user_id'] = '';
			$response['message'] = 'Invalid email or password';
		}
	}	
	
	echo json_encode($response);exit;		
} 

/*==============Register user screen=============*/

add_filter("wcra_registerUser_callback" , "wcra_registerUser_callback_handler");
function wcra_registerUser_callback_handler($param)
{
	global $wpdb;			
	$email = $param['email'];
	$password = base64_decode($param['password']);	
	if($email == '')
	{
		$response['success'] = 'false';
		$response['user_id'] = '';
		$response['message'] = 'Email required';
	}
	elseif($password == '')
	{
		$response['success'] = 'false';
		$response['user_id'] = '';
		$response['message'] = 'Password required';
	}
	else
	{
		$userdata = $wpdb->get_row("SELECT * FROM c1nqu3_users WHERE user_email = '".$email."'");
		if ($userdata != '') 
		{
			$response['success'] = 'false';
			$response['user_id'] = '';
			$response['message'] = 'Email already exist';
		}
		else
		{
			$wpEmail = explode("@",$email);

	    	$user_login = wp_slash($wpEmail[0]);
		    $user_email = wp_slash($email);
		    $user_pass  = $password;

		   	$wpdata = compact( 'user_login', 'user_email', 'user_pass' );
		    $user_id = wp_insert_user( $wpdata );
		    if($user_id != '' || $user_id != 0)
		    {
		    	$response['success'] = 'true';
		    	$response['user_id'] = "$user_id";
				$response['message'] = 'User registered successfully';
		    }
		    else
		    {
		    	$response['success'] = 'false';
		    	$response['user_id'] = '';
				$response['message'] = 'Error while registered.';
		    }
		}
	}
	echo json_encode($response);exit;
}

/*=============get locations for home page==============*/

add_filter("wcra_getLocations_callback" , "wcra_getLocations_callback_handler");
function wcra_getLocations_callback_handler($param)
{
	global $wpdb;
	$loc = get_field('homepage_locations','options');
	if (!empty($loc))
	{
		foreach ($loc as $key=>$l)
		{
			$location[$key]['location'] = $l['location'];
			$location[$key]['url'] = $l['url'];
			$location[$key]['image'] = wp_get_attachment_url( $l['image'],'thumbnail');
			if($l['starting_price'] != '')
			{
				$location[$key]['price'] = '€'.' '.$l['starting_price'];
			}
			else
			{
				$location[$key]['price'] = '';
			}
		}
		$response['success'] = 'true';
		$response['locations'] = $location;
	} 
	else
	{
		$response['success'] = 'false';
		$response['message'] = 'No record found';
	}
	echo json_encode($response);exit;
}

/*===============home page api===============*/

add_filter("wcra_homePage_callback" , "wcra_homePage_callback_handler");
function wcra_homePage_callback_handler($param)
{
	/*=====booking experience api=====*/

	$loc = get_field('homepage_locations','options');
	

	$location = array();
	if (!empty($loc))
	{
		foreach ($loc as $key=>$l)
		{
			$url=explode("/",$l['url']);
			if($url[4] != '')
			{
				$locs[$key]['slug'] = $url[4];
			}
			else
			{
				$locs[$key]['slug'] = '';
			}
			$locs[$key]['title'] = $l['location'];
			$locs[$key]['image'] = wp_get_attachment_url( $l['image'],'thumbnail');
			if($l['starting_price'] != '')
			{
				$locs[$key]['price'] = '€'.' '.$l['starting_price'];
			}
			else
			{
				$locs[$key]['price'] = '';
			}
		}
		$location = $locs;
	} 
	
	/*========activity api=======*/

	$act = get_field('activities_details','options');
	$activity = array();
	if (!empty($act))
	{
		foreach ($act as $key=>$l)
		{

			$acts[$key]['title'] = $l['title'];
			$acts[$key]['url'] = $l['url'];
			$acts[$key]['image'] = wp_get_attachment_url( $l['image'],'thumbnail');
			
		}
		
		if($param['limit'] != '')
		{
			$activity = array_slice($acts, 0, 3, true);
		}
		else
		{
			$activity = $acts;
		}
		
	} 

	/*===============what to do api============*/

	$what = get_field('homepage_todo_row_1','options');
	$whatToDo = array();
	if (!empty($what))
	{
		foreach ($what as $key=>$l)
		{
			$url=explode("/",$l['url']);
			if($url[3] != '')
			{
				$whatDo[$key]['slug'] = $url[3];
			}
			else
			{
				$whatDo[$key]['slug'] = '';
			}
			$whatDo[$key]['title'] = $l['title'];
			$whatDo[$key]['image'] = wp_get_attachment_url( $l['image'],'thumbnail');
			
		}
		$whatToDo = $whatDo;
	}

	/*===========Blogs==========*/

	$post_data= array('post_type' => 'post','posts_per_page'=> -1,'post__not_in' => get_option( 'sticky_posts' ) ); 
    $post_data = new WP_Query( $post_data );
    $blogs = array();
    if(!empty($post_data->posts))
    {
    	foreach($post_data->posts as $key=>$getPost)
    	{
    		$image = get_the_post_thumbnail_url($getPost->ID, 'full');
    		$postData[$key]['title'] = get_the_title($getPost->ID);
    		if($image != '')
    		{
    			$postData[$key]['image'] = get_the_post_thumbnail_url($getPost->ID, 'full');
    		}
    		else
    		{
    			$postData[$key]['image'] = "";
    		}
    		
    		//$postData[$key]['short_description'] = substr($getPost->post_content, 0, 100);
    		$postData[$key]['slug'] = $getPost->post_name;
    		//$postData[$key]['slug'] = get_site_url().'/'.$getPost->post_name.'/';
    	}
    	$blogs = $postData;

    }
    
	/*==============videos===========*/ 

	$getVideo = get_field('list_video','options');
	$video = array();
	if (!empty($getVideo))
	{
		foreach ($getVideo as $key=>$l)
		{
			preg_match('/src="([^"]+)"/', $l['video_url'], $match);
			$videourl = $match[1];

			$data[$key]['title'] = $l['video_title'];
			$data[$key]['image'] = wp_get_attachment_url( $l['video_image']['ID']);
			$data[$key]['video'] = $videourl;
		}
		$videos = $data;
	}

	/*========how to reach us=========*/

	$reach = get_field('home_reach_transportation','option');
	$howReach = array();
	if (!empty($reach))
	{
		foreach ($reach as $key=>$l)
		{
			$url=explode("/",$l['url']);
			$howsRe[$key]['title'] = $l['name'];
			if($url[4] == '' && $url[3] != '')
			{
				$howsRe[$key]['slug'] = $url[3];
			}
			else if($url[4] != '' && $url[3] != '')
			{
				$howsRe[$key]['slug'] = $url[4];
			}
			else
			{
				$howsRe[$key]['slug'] = '';
			}
			$howsRe[$key]['image'] = wp_get_attachment_url( $l['image'],'medium');
		}
		$howReach = $howsRe;
	}
	$response['success'] = true;
	$response['location'] = $location;
	$response['activity'] = $activity;
	$response['whatToDo'] = $whatToDo;
	$response['videos'] = $videos;
	$response['howReach'] = $howReach;
	$response['blogs'] = $blogs;
	$response['lat'] = '44.134660';
	$response['long'] = '9.683730';
	$response['banner_title'] = 'Do Not Miss Our Exclusive Promos.';
	$response['banner_sub_title'] = 'Unbeatable Cinque Terre Riviera Offers dedicated to all our guests. Surf through our offers and find the one that fits your stay';
	$response['banner_image'] = content_url().'/themes/cinqueterrerivierra-Newchild1234/new-assets/images/bg-promo.jpg';
	echo json_encode($response);exit;
}



/*================What To Do api for home page=================*/

add_filter("wcra_whatToDo_callback" , "wcra_whatToDo_callback_handler");
function wcra_whatToDo_callback_handler($param)
{
	$loc = get_field('homepage_todo_row_1','options');
	if (!empty($loc))
	{
		foreach ($loc as $key=>$l)
		{
			$data[$key]['title'] = $l['title'];
			$data[$key]['image'] = wp_get_attachment_url( $l['image'],'thumbnail');
		}
		$response['success'] = 'true';
		$response['whatToDo'] = $data;
	} 
	else
	{
		$response['success'] = 'false';
		$response['message'] = 'No record found';
	}
	echo json_encode($response);exit;
}

/*=============Get Location properties by location name api===============*/

add_filter("wcra_locationProperties_callback" , "wcra_locationProperties_callback_handler");
function wcra_locationProperties_callback_handler($param)
{
	global $wpdb;
	if(isset($param['name']) && !empty($param['name']))
	{
		$arr = array();
		$location = $param['name'];
		$getLocationDetail = $wpdb->get_results("SELECT c1nqu3_terms.*,c1nqu3_term_taxonomy.* FROM c1nqu3_terms INNER JOIN c1nqu3_term_taxonomy ON c1nqu3_terms.term_id = c1nqu3_term_taxonomy.term_id WHERE c1nqu3_terms.slug = '".$location."' AND c1nqu3_term_taxonomy.taxonomy = 'locations'");
		$term_id = $getLocationDetail[0]->term_id;
		if($term_id != '')
		{
			$term      = get_term( $term_id, 'locations' );
			$prefix    = 'cq_';
		  	$style     = (!empty(cq_taxonomy_image_url($term_id))) ? 'background: url('.cq_taxonomy_image_url($term_id).');' : '';
		 	$term      = $getLocationDetail[0];
		 	$term_name = $term->name;
			$current_location_desc   = $term->description;
		 	$presenter_custom_fields = get_option( "taxonomy_term_$term_id" ); 
			$presenter_data          = $presenter_custom_fields['map_url'];
			$f_location      = (!empty($_REQUEST['location'])) ? $_REQUEST['location'] : $term->term_id;
		  	$timestamp_start = (!empty($_REQUEST['from'])) ? strtotime(str_replace('/', '-', $_REQUEST['from'])) : strtotime('Today'); 
		  	$timestamp_end   = (!empty($_REQUEST['to'])) ? strtotime(str_replace('/', '-', $_REQUEST['to'])) : strtotime('Today'); 
		  	$types           = (!empty($_REQUEST['type'])) ? explode(',', $_REQUEST['type']) : array();
		  	$getamenities    = (!empty($_REQUEST['amn'])) ? explode(',', $_REQUEST['amn']) : array();
			$getprice        = (!empty($_REQUEST['price'])) ? explode(',', $_REQUEST['price']) : array();
			$displayDetail = false;
		  	if(false != ( get_request('bed') || get_request('bed') ) || !empty($types) || !empty($getamenities) || !empty($getprice))
		    	$displayDetail = true;
				$exclude = false;
		  	if(!empty($_REQUEST['from']) && !empty($_REQUEST['to']))
		  	{
		    	global $wpdb;
			    $query = "SELECT m.product FROM ".$wpdb->posts." p LEFT JOIN
			          ( SELECT  
			            post_id, 
			            GROUP_CONCAT(if(meta_key = '_booking_product_id', meta_value, NULL)) AS 'product', 
			            GROUP_CONCAT(if(meta_key = '_booking_start', meta_value, NULL)) AS 'start', 
			            GROUP_CONCAT(if(meta_key = '_booking_end', meta_value, NULL)) AS 'end', 
			            GROUP_CONCAT(if(meta_key = '_booking_all_day', meta_value, NULL)) AS 'allady' 
			          FROM ".$wpdb->postmeta."
			          GROUP BY post_id ) m
			          ON m.post_id = p.ID
			          WHERE p.post_type = 'wc_booking' 
			          AND p.post_status in ('publish','complete','paid','confirmed','unpaid','pending-confirmation','cancelled','wc-partially-paid','future','draft','pending','private')
			          AND m.start >= ".date('YmdHis', $timestamp_start)." 
			          AND m.start <= ".date('YmdHis', $timestamp_end)." 
			          GROUP BY product";

			    $results = $wpdb->get_results($query);
			    $not_ids = wp_list_pluck( $results, 'product' );
			    $exclude = true;
		  	}
			$author_settings = get_option($term->name.'short_option_cp'); 
		  	$args = array('post_type' => 'product', 'posts_per_page' =>-1,'post_status' => 'publish', 'tax_query' => array( array( 'taxonomy' => 'locations','field' => 'slug','terms'=> $term->slug,)), );
		  	$current_array = array();
		  	$portfolio = new WP_Query( $args );    
		  	if( $portfolio->have_posts() ) 
		  	{
		  		while ( $portfolio->have_posts() ) : $portfolio->the_post(); 
		  			array_push($current_array, get_the_ID());
		  		endwhile; 
		  	} 
		  	$current_arr_size = sizeof($current_array);
		  	$reorder_arr_size = sizeof($author_settings['pid_array']);
			if(is_array($author_settings['pid_array']))
		  	{
			    $post_order_backend = $author_settings['pid_array'];
		  	}
		  	else
		  	{
		    	$post_order_backend = array();
		  	}
		  	if($current_arr_size>$reorder_arr_size)
		  	{
		    	$order_array = array_unique (array_merge ($post_order_backend, $current_array));
		  	}
		  	else 
		  	{
		    	$order_array = $author_settings['pid_array'];
		  	}

		  	if($param['rating'] != '')
		  	{
		  		$searchRating = $param['rating'];
		  	}
		  	else
		  	{
		  		$searchRating = '';
		  	}

		  	if($param['start_price'] != '' && $param['end_price'] != '')
		  	{
		  		$searchPrice = array($param['start_price'],$param['end_price']);
		  	}
		  	else
		  	{
		  		$searchPrice = array();
		  	}
			

		  	$ord = ( get_request('sort') == 'low') ? 'ASC' : 'DESC';
		  	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		  	$args = array(
			    'post_type' => 'product',
			    'posts_per_page' => -1,
			    'paged' => $paged,
			    'meta_key' => '_price',
			    'post__in' => $order_array, 
			    'orderby'=>'post__in',
			   	'meta_query' => array(
                    array(
                        'key' => 'star_rating',
                        'value' => $param['rating'],
                        'compare' => 'LIKE'
                    ),
                    array(
		                'key' => 'starts_from',
		                'value' => $searchPrice,
		                'compare' => 'BETWEEN'
		            )
                )
		  	);
		  	$properties = new WP_Query($args);

		  	
		    $msg = 'true';

		   	if(!empty($properties->posts))
		   	{
		   		foreach($properties->posts as $key=>$val)
		    	{

		    		$get_property_type = get_the_terms( $val->ID, 'property_type' );

		    		$property_type = $get_property_type[0]->name;
		    		$property_type_slug = $get_property_type[0]->slug;

		    		

		    		if($param['property_type'] != '' && $param['status'] == '' )
		    		{
		    			if($property_type_slug == $param['property_type'])
		    			{
		    				$msg = 'true';
		    			}
		    			else
		    			{
		    				$msg = 'false';
		    			}
		    		}

		    		if($param['status'] != '' && $param['property_type'] == '')
		    		{
		    			if ( get_post_meta( $val->ID, 'instant_book', true ) ) 
			    		{
			    			if($param['status'] == 'Instant Bookable')
			    			{
			    				$msg = 'true';
			    			}
			    			else
			    			{
			    				$msg = 'false';
			    			}
			    		}
			    		else
			    		{
			    			if($param['status'] == 'On Request')
			    			{
			    				$msg = 'true';
			    			}
			    			else
			    			{
			    				$msg = 'false';
			    			}
			    		}
		    		}

		    		if($param['status'] != '' && $param['property_type'] != '')
		    		{
		    			if ( get_post_meta( $val->ID, 'instant_book', true ) ) 
			    		{
			    			if($param['status'] == 'Instant Bookable' && $property_type_slug == $param['property_type'])
			    			{
			    				$msg = 'true';
			    			}
			    			else
			    			{
			    				$msg = 'false';
			    			}
			    		}
			    		else
			    		{
			    			if($param['status'] == 'On Request' && $property_type_slug == $param['property_type'])
			    			{
			    				$msg = 'true';
			    			}
			    			else
			    			{
			    				$msg = 'false';
			    			}
			    		}
		    		}


		    		if($msg == 'true')
		    		{
		    			$res[$key]['slug'] = $val->post_name;
			    		$res[$key]['title'] = get_the_title($val->ID);
			    		$res[$key]['rating'] = get_field( 'star_rating', $val->ID );
			    		/*=== Geting status ====*/

			    		if ( get_post_meta( $val->ID, 'instant_book', true ) ) 
			    		{
			    			$res[$key]['status'] = 'Instant Bookable';
			    		}
			    		else
			    		{
			    			$res[$key]['status'] = 'On Request';
			    		}

			    		/*=== Geting price ====*/

			    		$RU = new rentalsUnited();
			    		$property_id = get_post_meta( $val->ID, 'propert_id', true );
			    		
			    		
			    		$each_id    = get_post_meta( $val->ID, 'propert_id', true );
			            $cache_file = get_stylesheet_directory(). '/cache-files/api-cache-taxo-'.$property_id.'.json';

			            if ( ! file_exists( $cache_file ) ) 
			            {
			                $image_response = $RU->getRates( $each_id );
			                $image_response = $RU->json_cached_api_taxo( $image_response, $property_id );
			                $image_json     = json_encode( $image_response );
			                $array11        = json_decode( $image_json, true );
			            } 
			            else 
			            {
			                $json11  = file_get_contents( $cache_file );
			                $array11 = json_decode( $json11, true );
			            }
			  
			            $currentdate = date("Y-m-d");
			            $DatesPerDay = array();

			            if ( $array11['Status'] == 'Success' ) 
			            {
			                $season_size = sizeof( $array11["Prices"]["Season"] );
			              	if ( $season_size == '3' ) 
			              	{
				                $data = array(
				                    'date' => $array11["Prices"]["Season"]["@attributes"]["DateFrom"],
				                    'price'=> $array11["Prices"]["Season"]["Price"]
				                  );
				                  array_push( $DatesPerDay, $data );

			                    $data = array(
			                      'date' => $array11["Prices"]["Season"]["@attributes"]["DateTo"],
			                      'price'=> $array11["Prices"]["Season"]["Price"]
			                    );
			                  	array_push( $DatesPerDay, $data );
			              	} 
			              	else 
			              	{
			                    for ( $i=0; $i < $season_size+1; $i++ ) 
			                    {

			                      	if ( ! empty( $array11["Prices"]["Season"][$i]["@attributes"]["DateFrom"] ) ) {
			                          	$data = array(
				                            'date' => $array11["Prices"]["Season"][$i]["@attributes"]["DateFrom"],
				                            'price'=> $array11["Prices"]["Season"][$i]["Price"]
			                          	);
			                          	array_push( $DatesPerDay, $data );
			                      	}

			                      	if ( ! empty( $array11["Prices"]["Season"][$i]["@attributes"]["DateTo"] ) ) {
			                          	$data = array(
				                            'date' => $array11["Prices"]["Season"][$i]["@attributes"]["DateTo"],
				                            'price'=> $array11["Prices"]["Season"][$i]["Price"]
			                          	);
			                          	array_push( $DatesPerDay, $data );
			                      	} 
			                    }
			                }
			            }
			            $start_count = 1;
			           	$start_status = 0;
			            if ( ! empty ( $DatesPerDay ) ) 
			            {
			            	foreach ( $DatesPerDay as  $value ) 
			            	{
			            		
			            		if ( $value['date'] == $currentdate && $start_count == 1 ) 
			            		{
			            			$startprice = intval( $value['price'] );
			            			$start_status++; 
			            			$start_count++; 
			            		}
			            	} 
			            }
			            if ( $start_status == 0 ) 
			            {
				          	$start_price = explode('.', get_field( "starts_from",$val->ID ) );
				          	$startprice = $start_price[0];
				        }

				         $res[$key]['price'] = '€'.' '.$startprice;

				        /*====get icons detail====*/
				        
				        if ( ! empty ( $property_id ) ) 
				        {
							$cache_file = get_stylesheet_directory(). '/cache-files/api-cache-'.$property_id.'.json';
							if ( ! file_exists( $cache_file ) ) 
							{
			                    $image_response = $RU->getProperty( $property_id );
			                    $image_response = $RU->json_cached_api_results_property( $image_response, $property_id );
			                    $image_json     = json_encode( $image_response );
			                    $images_array   = json_decode( $image_json, true );
			                } 
			                else 
			                {
			                    $fetch_data   = file_get_contents( $cache_file );
			                    $images_array = json_decode( $fetch_data, true );
			                }
			            }
						$CompositionRoomAmenities = $images_array["Property"]['CompositionRoomsAmenities']['CompositionRoomAmenities'];
						$RoomIDS = array();
			            if( ! empty( $CompositionRoomAmenities ) ) {
			                foreach ( $CompositionRoomAmenities as $value ) {
			                    array_push( $RoomIDS, $value["@attributes"]["CompositionRoomID"] );
			                }
			               $NumRoomIDS = array_count_values($RoomIDS);
			            }
				       
				        if( isset( $NumRoomIDS[257] ) && ! empty( $NumRoomIDS[257] ) ) 
			        	{
			        		$rooms =  $NumRoomIDS[257];
			        	}
			        	else
			        	{
			        		$rooms = '-';
			        	}
			        	if( isset( $NumRoomIDS[81] ) && ! empty( $NumRoomIDS[81] ) ) 
			        	{
			                $bathrooms =  $NumRoomIDS[81];
			        	}
			        	else
			        	{
			        		$bathrooms = '-';
			        	}

						$res[$key]['icons'] = array(
			        		'guests'=> (!empty(get_post_meta($val->ID, 'max_guest', true))) ? get_post_meta($val->ID, 'max_guest', true) : '-',
			        		'rooms'=> $rooms,
			        		'bathrooms'=> $bathrooms,
			        	);

			        	/*=======get gallery=====*/

			        	$images =  array();
						if ( isset( $images_array ) && ! empty ( $images_array ) ) 
			        	{
			        		$image_count = 1;
							foreach ( $images_array['Property']['Images']['Image'] as  $value ) 
			                {
			                    if ( $image_count < 4 ) 
			                    {
			                    	$images[] = $value;
			                    	$image_count++;
			                    }
			                }
			        	}
			        	$res[$key]['gallery'] =  $images;
			        	if($param['start_price'] != '' && $param['end_price'] != '')
			        	{
			        		if($startprice >= $param['start_price'] && $startprice <= $param['end_price'])
			        		{
			        			$res =  $res;
			        		}
			        		else
			        		{
			        			$res = array();
			        		}
			        	}
		    		}
				}
				if(!empty($res))
				{
					$arr = array_values($res);
				}
				else
				{
					$arr = array();
				}
			
			}
			$terms = array();
			$terms = get_terms([
			    'taxonomy' => 'property_type',
			    'hide_empty' => false,
			]);
		    $response['success'] = true;
			$response['locationProperties'] = $arr;
			$response['propertyType'] = $terms;
			$response['message'] = 'Success';
		}
		else
		{
			$response['success'] = false;
			$response['locationProperties'] = array();
			$response['propertyType'] = array();
			$response['message'] = 'Invalid Location';
		}
	}
	else
	{
		$response['success'] = false;
		$response['locationProperties'] = array();
		$response['propertyType'] = array();
		$response['message'] = 'Location parameter missing';
	}
    echo json_encode($response);exit;
}

/*==============What to do detail page============*/

add_filter("wcra_whatToDoDetail_callback" , "wcra_whatToDoDetail_callback_handler");
function wcra_whatToDoDetail_callback_handler($param)
{
	global $wpdb;
	$name =  $param['name'];
	
	$postdata = $wpdb->get_row("SELECT * FROM c1nqu3_posts WHERE post_name = '".strtolower($name)."'");
	$data = array();
	libxml_use_internal_errors(true);
	if(isset($param['name']) && $param['name'] != '')
	{
		if($postdata->post_name != '')
		{
			$data['banner_image'] = '';
			$feat_image = wp_get_attachment_url( get_post_thumbnail_id($postdata->ID) );
			if(!empty($feat_image))
			{
				$data['banner_image'] = $feat_image;
			}
			$data['banner_title'] = get_the_title($postdata->ID);
			$new = wp_specialchars_decode( $postdata->post_content,$quote_style = ENT_NOQUOTES );
			
			$html = '<div class="article">'. mb_convert_encoding($new,'HTML-ENTITIES','utf-8').'</div>';
		
			$dom = new DOMDocument();
			$dom->loadHTML($html);
			$xpath = new DOMXpath($dom);
			foreach ($xpath->query('//div[contains(@class, "article")]//img') as $img) {

			  $new_img = replace($img, $width = '350', $height = '200');

			  $replacement = $dom->createDocumentFragment();
			  $replacement->appendXML($new_img);

			  $img->parentNode->replaceChild($replacement, $img);

			}

			$new_html = $dom->saveXml();
			$newContent = do_shortcode($new_html);
			$data['content'] = remove_post_image_class($newContent);
		}
		$response['success'] = true;
		$response['message'] = 'Success';
		$response['detail'] = $data;
	}
	else
	{
		$response['success'] = true;
		$response['message'] = 'Parameter missing';
		$response['detail'] = $data;
	}
	echo json_encode($response);exit;
}

/*=============How To reach detail page api==============*/

add_filter("wcra_howToReachDetail_callback" , "wcra_howToReachDetail_callback_handler");
function wcra_howToReachDetail_callback_handler($param)
{
	global $wpdb;
	$name =  $param['name'];
	
	$postdata = $wpdb->get_row("SELECT * FROM c1nqu3_posts WHERE post_name = '".strtolower($name)."'");
	$data = array();
	libxml_use_internal_errors(true);
	if(isset($param['name']) && $param['name'] != '')
	{
		if($postdata->post_name != '')
		{
			$data['banner_image'] = '';
			$feat_image = wp_get_attachment_url( get_post_thumbnail_id($postdata->ID) );
			if(!empty($feat_image))
			{
				$data['banner_image'] = $feat_image;
			}
			$data['banner_title'] = get_the_title($postdata->ID);
			$new = wp_specialchars_decode( $postdata->post_content,$quote_style = ENT_NOQUOTES );
			
			$html = '<div class="article">'. mb_convert_encoding($new,'HTML-ENTITIES','utf-8').'</div>';
		
			$dom = new DOMDocument();
			$dom->loadHTML($html);
			$xpath = new DOMXpath($dom);
			foreach ($xpath->query('//div[contains(@class, "article")]//img') as $img) {

			  $new_img = replace($img, $width = '350', $height = '200');

			  $replacement = $dom->createDocumentFragment();
			  $replacement->appendXML($new_img);

			  $img->parentNode->replaceChild($replacement, $img);

			}

			$new_html = $dom->saveXml();
			$newContent = do_shortcode($new_html);
			$data['content'] = remove_post_image_class($newContent);
		}
		$response['success'] = true;
		$response['message'] = 'Success';
		$response['detail'] = $data;
	}
	else
	{
		$response['success'] = true;
		$response['message'] = 'Parameter missing';
		$response['detail'] = $data;
	}
	
	echo json_encode($response);exit;
}

/*===============Faq api=============*/

add_filter("wcra_faq_callback" , "wcra_faq_callback_handler");
function wcra_faq_callback_handler($param)
{
	$args = array('post_type' => 'faq','posts_per_page' => -1,);
	$faqs = new WP_Query($args);
	$data = array();
	if(!empty($faqs->posts))
	{
		foreach($faqs->posts as $key=>$val)
		{
			$data[$key]['title'] = $val->post_title;
			$data[$key]['content'] = $val->post_content;
		}
	}
	$response['success'] = true;
	$response['banner_title'] = 'F.A.Q'; 
	$response['banner_sub_title'] = 'Frequently asked questions'; 
	$response['banner_image'] = 'https://cinqueterreriviera.com/wp-content/uploads/2015/06/P9190260-e1486919660356.jpg'; 
	$response['detail'] = $data;
	echo json_encode($response);exit;
}

/*=============Blog detail page api============*/

function replace($img, $width, $height) {

  	$href = $img->getAttribute('src');
  	$alt = $img->getAttribute('alt');
	$new_img = '<br/><img src="'.$href.'" width= "'.$width.'" height= "'.$height.'" align="middle"/><br/>';

  	return $new_img;
}

add_filter("wcra_blogDetail_callback" , "wcra_blogDetail_callback_handler");
function wcra_blogDetail_callback_handler($param)
{
	global $wpdb;
	$name =  $param['name'];

	$postdata = $wpdb->get_row("SELECT * FROM c1nqu3_posts WHERE post_name = '".strtolower($name)."'");
	$data = array();
	libxml_use_internal_errors(true);
	if(isset($param['name']) && $param['name'] != '')
	{
		if($postdata->post_name != '')
		{
			
			$data['banner_image'] = '';
			$feat_image = wp_get_attachment_url( get_post_thumbnail_id($postdata->ID) );
			if(!empty($feat_image))
			{
				$data['banner_image'] = $feat_image;
			}
			$data['banner_title'] = get_the_title($postdata->ID);
			$new = wp_specialchars_decode( $postdata->post_content,$quote_style = ENT_NOQUOTES );
			
			$html = '<div class="article">'. mb_convert_encoding($new,'HTML-ENTITIES','utf-8').'</div>';
		
			$dom = new DOMDocument();
			$dom->loadHTML($html);
			$xpath = new DOMXpath($dom);
			foreach ($xpath->query('//div[contains(@class, "article")]//img') as $img) {

			  $new_img = replace($img, $width = '350', $height = '200');

			  $replacement = $dom->createDocumentFragment();
			  $replacement->appendXML($new_img);

			  $img->parentNode->replaceChild($replacement, $img);

			}

			$new_html = $dom->saveXml();
			$newContent = do_shortcode($new_html);
			$data['content'] = remove_post_image_class($newContent);
		}
		$response['success'] = true;
		$response['message'] = 'Success';
		$response['detail'] = $data;
	}
	else
	{
		$response['success'] = true;
		$response['message'] = 'Parameter missing';
		$response['detail'] = $data;
	}
	
	echo json_encode($response);exit;
}

/*===============blog list api=============*/

add_filter("wcra_blogList_callback" , "wcra_blogList_callback_handler");
function wcra_blogList_callback_handler($param)
{

	$cat = $param['category'];
	$blogs = array();
	if(isset($cat) && $cat != '')
	{
		$catDetail = get_term_by( 'name', $cat, 'category' );
		$catID = $catDetail->term_id;
		$args = array('category_name' => $cat );
	    $posts = get_posts( $args );
	    if(!empty($posts))
	    {
	    	foreach($posts as $key=>$getPost)
	    	{
	    		$image = get_the_post_thumbnail_url($getPost->ID, 'full');
	    		$postData[$key]['title'] = get_the_title($getPost->ID);
	    		if($image != '')
	    		{
	    			$postData[$key]['image'] = get_the_post_thumbnail_url($getPost->ID, 'full');
	    		}
	    		else
	    		{
	    			$postData[$key]['image'] = "";
	    		}
	    		
	    		$postData[$key]['short_description'] = substr($getPost->post_content, 0, 300).'[...]';
	    		$postData[$key]['slug'] = $getPost->post_name;
	    		$postData[$key]['date'] = get_the_date('d M',$getPost->ID);
	    	}
	    	$blogs['banner_title'] = "";
	    	$blogs['banner_image'] = "";
	    	$blogs['content'] = "";

	    	$banner_title = get_field('header_title',$catDetail);
	    	$banner_image = get_field('header_image',$catDetail);
	    	$content = get_field('header_content',$catDetail);

	    	$blogs['blog_description'] = $postData;
	    	if($banner_title != "")
	    	{
	    		$blogs['banner_title'] = get_field('header_title',$catDetail);
	    	}
	    	if($banner_image != "")
	    	{
	    		$blogs['banner_image'] = get_field('header_image',$catDetail);
	    	}
	    	if($content != "")
	    	{
	    		$blogs['content'] = get_field('header_content',$catDetail);
	    	}
	    }
		$response['success'] = true;
		$response['message'] = 'Success';
		$response['detail'] = $blogs;
	}
	else
	{
		$response['success'] = true;
		$response['message'] = 'Parameter missing';
		$response['detail'] = $blogs;
	}
	
	echo json_encode($response);exit;
}

/*===============Contact Us api=============*/

function remove_post_image_class($content) {
    $post_string = $content;
    $post_string = preg_replace('/<figure[^>]+\>/i', '', $post_string);
   	return $post_string;
}

add_filter( 'the_content', 'remove_post_image_class' );

add_filter("wcra_contactUs_callback" , "wcra_contactUs_callback_handler");
function wcra_contactUs_callback_handler($param)
{
	$page = get_posts(
	    array(
	        'name'      => 'contact-us',
	        'post_type' => 'page'
	    )
	);
	$data = array();
	libxml_use_internal_errors(true);
	if(!empty($page[0]))
	{
		$data['banner_image'] = '';
		$feat_image = wp_get_attachment_url( get_post_thumbnail_id($page[0]->ID) );
		if(!empty($feat_image))
		{
			$data['banner_image'] = $feat_image;
		}
		$data['banner_title'] = get_the_title($page[0]->ID);
		$new = wp_specialchars_decode( $page[0]->post_content,$quote_style = ENT_NOQUOTES );

		$html = '<div class="article">'. mb_convert_encoding($new,'HTML-ENTITIES','utf-8').'</div>';
		
		$dom = new DOMDocument();
		$dom->loadHTML($html);
		$xpath = new DOMXpath($dom);
		foreach ($xpath->query('//div[contains(@class, "article")]//img') as $img) {

		  $new_img = replace($img, $width = '350', $height = '200');

		  $replacement = $dom->createDocumentFragment();
		  $replacement->appendXML($new_img);

		  $img->parentNode->replaceChild($replacement, $img);

		}

		$new_html = $dom->saveXml();
		$newContent = do_shortcode($new_html);
		$data['content'] = remove_post_image_class($newContent);
	}
	$response['success'] = true;
	$response['message'] = 'Success';
	$response['detail'] = $data;
	echo json_encode($response);exit;
}

/*================Service menu api==============*/

add_filter("wcra_serviceMenu_callback" , "wcra_serviceMenu_callback_handler");
function wcra_serviceMenu_callback_handler($param)
{
	global $wpdb;
	$prop = array();
	libxml_use_internal_errors(true);
	if($param['url'] != '')
	{
		if($param['url'] == 'travelling-with-us')
        {
        	
        	//$getname =  explode("/",$param['url']);
        	$name = $param['url'];

			$postdata = $wpdb->get_row("SELECT * FROM c1nqu3_posts WHERE post_name = '".$name."'");
			$prop = array();
			
			if($postdata->post_name != '')
			{
				$prop['banner_image'] = '';
				$feat_image = wp_get_attachment_url( get_post_thumbnail_id($postdata->ID) );
				if(!empty($feat_image))
				{
					$prop['banner_image'] = $feat_image;
				}
				$prop['banner_title'] = get_the_title($postdata->ID);
				//AAKASH 
			/*$new = wp_specialchars_decode( $postdata->post_content,$quote_style = ENT_NOQUOTES );
			$new_string = breezer_addDivToImage(apply_filters('the_content', $new));	
			$html = '<div class="article">'. mb_convert_encoding($new_string,'HTML-ENTITIES','utf-8').'</div>';
				//END AAKASH
				// $new_string = breezer_addDivToImage(apply_filters('the_content', $postdata->post_content));
				// $html = '<div class="article">'.$new_string.'</div>';
	
				$dom = new DOMDocument();
				$dom->loadHTML($html);
				$xpath = new DOMXpath($dom);
				foreach ($xpath->query('//div[contains(@class, "article")]//img') as $img) {

				  $new_img = replace($img, $width = '350', $height = '200');

				  $replacement = $dom->createDocumentFragment();
				  $replacement->appendXML($new_img);

				  $img->parentNode->replaceChild($replacement, $img);

				}

				$new_html = $dom->saveXml();

				$prop['content'] = $new_html;*/


				$new = wp_specialchars_decode( $postdata->post_content,$quote_style = ENT_NOQUOTES );
			
				$html = '<div class="article">'. mb_convert_encoding($new,'HTML-ENTITIES','utf-8').'</div>';
			
				$dom = new DOMDocument();
				$dom->loadHTML($html);
				$xpath = new DOMXpath($dom);
				foreach ($xpath->query('//div[contains(@class, "article")]//img') as $img) {

				  $new_img = replace($img, $width = '350', $height = '200');

				  $replacement = $dom->createDocumentFragment();
				  $replacement->appendXML($new_img);

				  $img->parentNode->replaceChild($replacement, $img);

				}

				$new_html = $dom->saveXml();
				$newContent = do_shortcode($new_html);
				$prop['content'] = remove_post_image_class($newContent);
			}
			$response['success'] = true;
			$response['message'] = 'Success';
			$response['detail'] = $prop;
        }
       	elseif($param['url'] == 'wedding-proposal-in-the-cinque-terre')
        {
        	//$getname =  explode("/",$param['url']);

			$name = $param['url'];

			$postdata = $wpdb->get_row("SELECT * FROM c1nqu3_posts WHERE post_name = '".$name."'");
			$prop = array();
			
			if($postdata->post_name != '')
			{
				$prop['banner_image'] = '';
				$feat_image = wp_get_attachment_url( get_post_thumbnail_id($postdata->ID) );
				if(!empty($feat_image))
				{
					$prop['banner_image'] = $feat_image;
				}
				$prop['banner_title'] = get_the_title($postdata->ID);
				/*$new_string = breezer_addDivToImage(apply_filters('the_content', $postdata->post_content));
				$html = '<div class="article">'.$new_string.'</div>';
	
				$dom = new DOMDocument();
				$dom->loadHTML($html);
				$xpath = new DOMXpath($dom);
				foreach ($xpath->query('//div[contains(@class, "article")]//img') as $img) {

				  $new_img = replace($img, $width = '350', $height = '200');

				  $replacement = $dom->createDocumentFragment();
				  $replacement->appendXML($new_img);

				  $img->parentNode->replaceChild($replacement, $img);

				}

				$new_html = $dom->saveXml();

				$prop['content'] = $new_html;*/
				$new = wp_specialchars_decode( $postdata->post_content,$quote_style = ENT_NOQUOTES );
			
				$html = '<div class="article">'. mb_convert_encoding($new,'HTML-ENTITIES','utf-8').'</div>';
			
				$dom = new DOMDocument();
				$dom->loadHTML($html);
				$xpath = new DOMXpath($dom);
				foreach ($xpath->query('//div[contains(@class, "article")]//img') as $img) {

				  $new_img = replace($img, $width = '350', $height = '200');

				  $replacement = $dom->createDocumentFragment();
				  $replacement->appendXML($new_img);

				  $img->parentNode->replaceChild($replacement, $img);

				}

				$new_html = $dom->saveXml();
				$newContent = do_shortcode($new_html);
				$prop['content'] = remove_post_image_class($newContent);

			}
			$response['success'] = true;
			$response['message'] = 'Success';
			$response['detail'] = $prop;
        }
        elseif($param['url'] == 'wedding/vows-renewal-in-the-cinque-terre')
        {
        	//$getname =  explode("/",$param['url']);

			$name = $param['url'];

			$postdata = $wpdb->get_row("SELECT * FROM c1nqu3_posts WHERE post_name = 'vows-renewal-in-the-cinque-terre'");
			$prop = array();
			
			if($postdata->post_name != '')
			{
				$prop['banner_image'] = '';
				$feat_image = wp_get_attachment_url( get_post_thumbnail_id($postdata->ID) );
				if(!empty($feat_image))
				{
					$prop['banner_image'] = $feat_image;
				}
				$prop['banner_title'] = get_the_title($postdata->ID);
				$new = wp_specialchars_decode( $postdata->post_content,$quote_style = ENT_NOQUOTES );
			
				$html = '<div class="article">'. mb_convert_encoding($new,'HTML-ENTITIES','utf-8').'</div>';
			
				$dom = new DOMDocument();
				$dom->loadHTML($html);
				$xpath = new DOMXpath($dom);
				foreach ($xpath->query('//div[contains(@class, "article")]//img') as $img) {

				  $new_img = replace($img, $width = '350', $height = '200');

				  $replacement = $dom->createDocumentFragment();
				  $replacement->appendXML($new_img);

				  $img->parentNode->replaceChild($replacement, $img);

				}

				$new_html = $dom->saveXml();
				$newContent = do_shortcode($new_html);
				$prop['content'] = remove_post_image_class($newContent);
			}
			$response['success'] = true;
			$response['message'] = 'Success';
			$response['detail'] = $prop;
        }
        elseif($param['url'] == 'rental-agreement')
        {
        	//$getname =  explode("/",$param['url']);

			$name = $param['url'];
			$prop['pdf'] = get_bloginfo('wpurl').'/wp-content/uploads/2019/10/2018-Rental-Agreement-dal-15-feb-18.pdf';
			
			$response['success'] = true;
			$response['message'] = 'Success';
			$response['detail'] = $prop;
        }
		elseif($param['url'] == 'luxury-collection')
		{
			$luxury_author_settings = get_option('luxury_collshort_option_cp');
			if(isset($luxury_author_settings['pid_array']))
            {
                $args = array('post_type' => 'product', 'posts_per_page' =>-1,'post_status' => 'publish', 'meta_key' => 'luxury_st', 'meta_value' => 'on');
                $luxury_current_array = array();
                $portfolio = new WP_Query( $args );      
                if( $portfolio->have_posts() ) 
                {
	                while ( $portfolio->have_posts() ) : $portfolio->the_post(); 
	                    array_push($luxury_current_array, get_the_ID());
	                endwhile; 
	            } 

                $luxury_current_arr_size = sizeof($luxury_current_array);

                if(is_array($luxury_author_settings['pid_array']))
                {
                    $luxury_order_array = $luxury_author_settings['pid_array'];
                }
                else
                {
                    $luxury_order_array = array();
                }

                $luxury_reorder_arr_size = sizeof($luxury_order_array);

                if($luxury_current_arr_size>$luxury_reorder_arr_size)
                {
                    $order_array = array_unique (array_merge ($luxury_order_array, $luxury_current_array));
                }
                else 
                {
                    $order_array = $luxury_author_settings['pid_array'];
                }
                $args = array('post_type' => 'product', 'posts_per_page' =>-1,'post_status' => 'publish', 'post__in' => $order_array, 'orderby'=>'post__in', 'meta_key' => 'luxury_st', 'meta_value' => 'on' );
            }
            else
            {
                $args = array('post_type'=> 'product','posts_per_page' => -1, 'meta_key' => 'luxury_st', 'meta_value' => 'on',);
            }
            $loop = new WP_Query( $args );
           	if(!empty($loop->posts))
            {
            	foreach($loop->posts as $key=>$val)
            	{
            		$product = wc_get_product( $val->ID );
            		$property_id = get_post_meta( $val->ID, 'propert_id', true );
            		$cache_file  = get_stylesheet_directory(). '/cache-files/api-cache-taxo-'.$property_id.'.json';

		            if ( ! file_exists( $cache_file ) )
		            {
		                $image_response = $RU->getRates( $each_id );
		                $image_response = $RU->json_cached_api_taxo( $image_response, $property_id );
		                $image_json     = json_encode( $image_response );
		                $array11        = json_decode( $image_json, true );
		            } 
		            else 
		            {
		                $json11  = file_get_contents( $cache_file );
		                $array11 = json_decode( $json11, true );
		            }
  
       			 	$currentdate = date("Y-m-d");
            		$DatesPerDay = array();

		            /* Geting price */
		            if ( $array11['Status'] == 'Success' ) 
		            {
		                $season_size = sizeof( $array11["Prices"]["Season"] );
	                  	if ( $season_size == '3' ) 
	                  	{
		                    $data = array(
		                        'date' => $array11["Prices"]["Season"]["@attributes"]["DateFrom"],
		                        'price'=> $array11["Prices"]["Season"]["Price"]
		                      );
	                      	array_push( $DatesPerDay, $data );

	                      	$data = array(
		                        'date' => $array11["Prices"]["Season"]["@attributes"]["DateTo"],
		                        'price'=> $array11["Prices"]["Season"]["Price"]
	                      	);
	                      	array_push( $DatesPerDay, $data );
	                  	} 
	                  	else 
	                  	{
		                    for ( $i=0; $i < $season_size+1; $i++ ) 
		                    {

		                      	if ( ! empty( $array11["Prices"]["Season"][$i]["@attributes"]["DateFrom"] ) ) 
		                      	{
		                          	$data = array(
			                            'date' => $array11["Prices"]["Season"][$i]["@attributes"]["DateFrom"],
			                            'price'=> $array11["Prices"]["Season"][$i]["Price"]
		                          	);
		                          	array_push( $DatesPerDay, $data );
		                      	}

		                      	if ( ! empty( $array11["Prices"]["Season"][$i]["@attributes"]["DateTo"] ) ) 
		                      	{
		                          	$data = array(
			                            'date' => $array11["Prices"]["Season"][$i]["@attributes"]["DateTo"],
			                            'price'=> $array11["Prices"]["Season"][$i]["Price"]
		                          	);
		                          	array_push( $DatesPerDay, $data );
		                      	} 
		                    }
		                }
		            }
		            $start_count = 1;

		            /*= get status =*/

		            if(get_post_meta( $val->ID, 'instant_book', true )) 
            		{ 
            			$prop[$key]['status'] = 'Instant Bookable'; 
            		} 
            		else 
        			{ 
        				$prop[$key]['status'] = 'On Request'; 
        			}

        			/*= get price =*/

        			if ( ! empty ( $DatesPerDay ) ) 
        			{
                    	foreach ( $DatesPerDay as $value ) 
                    	{
                            if ( $value['date'] == $currentdate && $start_count == 1 ) 
                        	{ 
                                $prop[$key]['price'] = '€'.' '.intval( $value['price'] );
                            	$start_count++; 
                            } 
                            else
                            {
                            	$prop[$key]['price'] = '';
                            }
                        } 
                    }

                    /*= get title =*/

                    $prop[$key]['title'] = get_the_title($val->ID);

                    /*= get location =*/

                    $terms = get_the_terms( $val->ID , 'locations' );
					foreach ( $terms as $term ) 
					{
						$prop[$key]['location'] = $term->name;
					}

					/*===get image===*/

					$prop[$key]['image'] = get_the_post_thumbnail_url($val->ID);
					$prop[$key]['short_desc'] = get_the_excerpt($val->ID);
					$prop[$key]['slug'] = $val->post_name;

					$response['success'] = true;
					$response['message'] = 'Success';
					$response['detail'] = $prop;
				}
	        }
	    }
		else
        {
        	$response['success'] = true;
			$response['message'] = 'Success';
			$response['detail'] = $prop;
        }
    }
	else
	{
		$response['success'] = true;
		$response['message'] = 'Parameter missing';
		$response['detail'] = $prop;
	}

	echo json_encode($response);exit;
}

/*========property detail page api===========*/

add_filter("wcra_propertyDetail_callback" , "wcra_propertyDetail_callback_handler");
function wcra_propertyDetail_callback_handler($param)
{
	global $wpdb;
	$data = array();
	if(isset($param['name']) && $param['name'] != '')
	{
		$postdata = $wpdb->get_row("SELECT * FROM c1nqu3_posts WHERE post_name = '".$param['name']."'");
		if($postdata != '')
		{
			/*===get amenties===*/

			$term_obj_list = get_the_terms( $postdata->ID, 'amenity' );
			$amenties = array();
			if(!isset($term_obj_list->error_data))
			{
				foreach($term_obj_list as $amen)
				{
					$amenties[] = $amen->name;
				}
			}
			$data['amenties'] = $amenties;

			/*=====product tags near location=====*/

			$property_tags = get_the_terms( $postdata->ID, 'product_tag' );
			$proTags = array();
			if(!isset($property_tags->error_data))
			{
				foreach($property_tags as $tags)
				{
					$proTags[] = $tags->name;
				}
			}

			/*=====product details=====*/

			$property_type = get_the_terms( $postdata->ID, 'property_type' );
			$property_location = get_the_terms( $postdata->ID, 'locations' );
			

			$data['Property Type'] = '';
			$data['Property Location'] = '';
			if(!isset($property_type->error_data))
			{
				$data['Property Type'] = $property_type[0]->name;
			}

			if(!isset($property_location->error_data))
			{
				$data['Property Location'] = $property_location[0]->name;
			}

			$data['Title'] = $postdata->post_title;
			$data['Description'] = $postdata->post_content;
			$data['Product Tags'] = $proTags;
			$data['product_id'] = $postdata->ID;
			$data['property_id'] = get_post_meta( $postdata->ID, 'propert_id', true );

			
			
			
			/*====get icons detail====*/
					        
		    if ($postdata->ID) 
		    {
		    	$RU = new rentalsUnited();
		    	$property_id = get_post_meta( $postdata->ID, 'propert_id', true );

				$cache_file = get_stylesheet_directory(). '/cache-files/api-cache-'.$property_id.'.json';
				if ( ! file_exists( $cache_file ) ) 
				{
		            $image_response = $RU->getProperty( $property_id );
		            $image_response = $RU->json_cached_api_results_property( $image_response, $property_id );
		            $image_json     = json_encode( $image_response );
		            $images_array   = json_decode( $image_json, true );
		        } 
		        else 
		        {
		            $fetch_data   = file_get_contents( $cache_file );
		            $images_array = json_decode( $fetch_data, true );
		        }
		    }
		  	$guestName = array('Standard Guest','Max Guest');

			$guestVal = array($images_array["Property"]["StandardGuests"],$images_array["Property"]["CanSleepMax"]);
			

			$satndGuest = array();

			if(!empty($guestName))
			{
				foreach($guestName as $keys1=>$sg)
				{
					$satndGuest[$keys1]['name'] = $sg;
					$satndGuest[$keys1]['value'] = $guestVal[$keys1];
				}
			}

			//$prop_details = get_property_details( $postdata->ID ); 
			$prop_details = get_property_details( $postdata->ID); 
			$prop_val = array();
			foreach ( $prop_details as $key => $prop ) 
			{
				if ($prop['name'] == 'Max Guest'  ) 
				{ 

    			}
    			else
    			{
    				$prop_val[$key]['name'] = (is_array($prop['name'])) ? implode(', ', $prop['name']) : $prop['name'];
    				$prop_val[$key]['value'] = (is_array($prop['value'])) ? implode(', ', $prop['value']) : $prop['value'];
					
    			}
			}

			

			$arraym = array_merge($prop_val,$satndGuest);

			$data['details'] = $arraym;

			$nearbyname = array('Opera Festival','Pasta','Bakery Class','Wine Tastings','Pesto','Two Day Wine Tastings','Experience with Chef Silvia');

			$nearbyimage = array('https://cinqueterreriviera.com/wp-content/uploads/2019/03/MATT9908-1024x683.jpg','https://cinqueterreriviera.com/wp-content/uploads/2018/03/P1010031.jpg','https://cinqueterreriviera.com/wp-content/uploads/2018/03/P3090084.jpg','https://cinqueterreriviera.com/wp-content/uploads/2015/08/P8210393.jpg','https://cinqueterreriviera.com/wp-content/uploads/2018/03/P1010022.jpg','https://cinqueterreriviera.com/wp-content/uploads/2016/01/DSC_0756.jpg','https://cinqueterreriviera.com/wp-content/uploads/2017/03/P7160093.jpg');

			$nearbyslug = array('https://cinqueterreriviera.com/vernazza-opera-festival-2019/','https://cinqueterreriviera.com/pasta-making-experience/','https://cinqueterreriviera.com/chocolate-show-class/','https://cinqueterreriviera.com/wine-tasting/','https://cinqueterreriviera.com/pesto-experience-in-manarola/','https://cinqueterreriviera.com/two-day-wine-experience/','https://cinqueterreriviera.com/chef-silvia-master-cooking-experience/');

			$ny = array();

			if(!empty($nearbyname))
			{

				foreach($nearbyname as $keys2=>$ny)
				{

					$nearByT[$keys2]['name'] = $ny;
					$nearByT[$keys2]['image'] = $nearbyimage[$keys2];
					$nearByT[$keys2]['slug'] = $nearbyslug[$keys2];
				}
			}

			$data['nearby'] = $nearByT;

			$data['floorPlanImage'] = "";

			if ( ! empty ( get_field( "floor_plan_images" ) ) )
			{
				$floor_plans = get_field( "floor_plan_images" );
            	foreach ( $floor_plans as $key => $value )
              	{
                	$data['floorPlanImage'] = wp_get_attachment_url( $value['image'],'full');
              	}
            
			} 

			$CompositionRoomAmenities = $images_array["Property"]['CompositionRoomsAmenities']['CompositionRoomAmenities'];
			$RoomIDS = array();
		    if( ! empty( $CompositionRoomAmenities ) ) {
		        foreach ( $CompositionRoomAmenities as $value ) {
		            array_push( $RoomIDS, $value["@attributes"]["CompositionRoomID"] );
		        }
		       $NumRoomIDS = array_count_values($RoomIDS);
		    }
		   
		    if( isset( $NumRoomIDS[257] ) && ! empty( $NumRoomIDS[257] ) ) 
			{
				$rooms =  $NumRoomIDS[257];
			}
			else
			{
				$rooms = '-';
			}
			if( isset( $NumRoomIDS[81] ) && ! empty( $NumRoomIDS[81] ) ) 
			{
		        $bathrooms =  $NumRoomIDS[81];
			}
			else
			{
				$bathrooms = '-';
			}

			$data['icons'] = array(
				'guests'=> (!empty(get_post_meta($postdata->ID, 'max_guest', true))) ? get_post_meta($postdata->ID, 'max_guest', true) : '-',
				'rooms'=> $rooms,
				'bathrooms'=> $bathrooms,
			);
			
			$meta = get_post_meta($postdata->ID);

			/*===get our rules===*/

			$data['Our Rules'] = array(
				'Minimum Stay' =>  $meta['_house_rules_minimum_stay'][0],
				'Garbage Collection' =>  $meta['_house_rules_garbage_collection'][0],
				'Smoking' =>  $meta['_house_rules_smoking_allowed'][0],
				'Pets' =>  $meta['_house_rules_pets_allowed'][0],
				'Check-In Time' =>  $meta['_house_rules_checkin_time'][0],
				'Check-Out Time' =>  $meta['_house_rules_checkout_time'][0],
			);
			

			//$data['Our Rules'] = $ourrules;

			/*===get citr code===*/

			$data['Citra Code'] = $meta['citra_code'][0];

			/*====get extras===*/
			
			$getExtras = get_field('extras',$postdata->ID);
			/*$extras_name = array(
				"Cleaning Fees" => get_field('cleaning_fee',$postdata->ID), 
             	"Security Deposit" => $meta['_extra_price_info_security_deposit'][0],
             	"City Tax" => $meta['city_tax'][0],
             	"Deposit" => $meta['_wc_deposits_deposit_amount'][0].'% (if reservation is made more than 30 days before arrival)',
         	);*/
         	$extras_value = array(
				get_field('cleaning_fee',$postdata->ID), 
             	$meta['_extra_price_info_security_deposit'][0],
             	$meta['city_tax'][0],
             	$meta['_wc_deposits_deposit_amount'][0].'% (if reservation is made more than 30 days before arrival)',
         	);
         	$extras_name = array(
				"Cleaning Fees", 
             	"Security Deposit",
             	"City Tax",
             	"Deposit",
         	);

         	$myObject1 = array();
         	$myObject2 = array();

         	foreach($extras_name as $key => $value) 
			{
			    $myObject1[$key]['name'] = $value;
			    $myObject1[$key]['value'] = $extras_value[$key];
			}

			if(!empty($getExtras))
			{
				foreach($getExtras as $keys=>$ex)
				{
					$myObject2[$keys]['name'] = $ex['extra_item'];
					$myObject2[$keys]['value'] = $ex['extra_price'];
				}
			}
			$data['extras'] =  array_merge($myObject1, $myObject2);;

			/*====get season tariff=====*/

			$getTariff = unserialize(get_field('season_duration',$postdata->ID));
			$getTariffPrice = unserialize(get_field('season_amount',$postdata->ID));
			$getTariffMinStay = unserialize(get_field('min_stay',$postdata->ID));
			$getSeasonTariff = array();
			foreach($getTariff as $key=>$val)
			{
				$getSeasonTariff[$key]['Property Rates'] = $val;
				$getSeasonTariff[$key]['Daily'] = '€'.' '.$getTariffPrice[$key];
				$getSeasonTariff[$key]['Min Stay'] = $getTariffMinStay[$key];
			}

			$data['Property Rates'] = $getSeasonTariff;

			/*====get location lat and long====*/

			$data['Location'] = array(
				'Lat' => $meta['latidude'][0],
				'Long' => $meta['longitude'][0]
			);

			/*========get comments=====*/

			$sql = "SELECT * FROM $wpdb->comments LEFT OUTER JOIN $wpdb->posts ON ($wpdb->comments.comment_post_ID = $wpdb->posts.ID) WHERE $wpdb->posts.ID='".$postdata->ID."' ORDER BY comment_date_gmt DESC";
		 
			$getComments = $wpdb->get_results($sql);
			$comments = array();
			if(!empty($getComments))
			{
				foreach($getComments as $ky=>$comm)
				{
					$comments[$ky]['name'] = $comm->comment_author;
					$comments[$ky]['comment'] = $comm->comment_content;
				}
			}
			$data['comments'] = $comments;

			/*=====get gallery====*/

			$images =  array();
			if ( isset( $images_array ) && ! empty ( $images_array ) ) 
	    	{
	    		$image_count = 1;
				foreach ( $images_array['Property']['Images']['Image'] as  $value ) 
	            {
	                if ( $image_count < 4 ) 
	                {
	                	$images[] = $value;
	                	$image_count++;
	                }
	            }
	    	}
	    	$data['gallery'] =  $images;
	    	$data['rating'] = get_field( 'star_rating', $postdata->ID );

			$response['success'] = true;
			$response['message'] = 'Success';
			$response['detail'] = $data;
		}
		else
		{
			$response['success'] = true;
			$response['message'] = 'Success';
			$response['detail'] = $data;
		}
	}
	else
	{
		$response['success'] = true;
		$response['message'] = 'Parameter missing';
		$response['detail'] = $data;
	}
	echo json_encode($response);exit;
}

/*=============subscription api=======*/

add_filter("wcra_newsletterSubscription_callback" , "wcra_newsletterSubscription_callback_handler");
function wcra_newsletterSubscription_callback_handler($param)
{
	global $wpdb;
	
	if(isset($param['email']) && $param['email'] != '')
	{
		$sql = $wpdb->get_row("SELECT * FROM c1nqu3_options WHERE option_name = 'subscription_mail_list'");
		$getEmails = unserialize($sql->option_value);
		$newEmail = array('email' => $param['email']);
		array_push($getEmails, $newEmail);
		$allEmails = serialize($getEmails);
		"UPDATE c1nqu3_options SET option_value = '".$allEmails."' WHERE option_name = 'subscription_mail_list'";
		
		$data = [ 'option_value' => $allEmails ];
		$where = [ 'option_name' => 'subscription_mail_list' ];
		$wpdb->update( $wpdb->c1nqu3_options , $data, $where );
		//print_r($res);die;
	}
	else
	{
		$response['success'] = false;
		$response['message'] = 'Email required';
	}
	echo json_encode($response);exit;
}

/*=================Profile detail============*/

add_filter("wcra_profileDetail_callback" , "wcra_profileDetail_callback_handler");
function wcra_profileDetail_callback_handler($param)
{
	$userId = $param['userId'];
	$data = array();
	if(isset($userId) && $userId != '')
	{
		$profile = get_userdata( $userId ); 
		$image =  wp_get_attachment_url( get_user_meta( $userId, 'img_profile', true ), 'full' );
		if($image)
		{
			$data['image'] = $image;
		}
		else
		{
			$data['image'] = "";
		}
		$data['username'] = $profile->user_login;
		$data['firstname'] = get_user_meta( $userId, 'billing_first_name', true );  
		$data['lastname'] = get_user_meta( $userId, 'billing_last_name', true );  
		$data['email'] = get_user_meta( $userId, 'billing_email', true );  
		$data['phonenumber'] = get_user_meta( $userId, 'billing_phone', true );  
		$data['address'] = get_user_meta( $userId, 'billing_address_1', true );  
		$data['city'] = get_user_meta( $userId, 'billing_city', true );  
		$data['Country'] = get_user_meta( $userId, 'billing_country', true ); 

		$response['success'] = true;
		$response['message'] = 'Success';
		$response['detail'] = $data;
	}
	else
	{
		$response['success'] = false;
		$response['message'] = 'Parameter Missing';
		$response['detail'] = $data;
	}
	echo json_encode($response);exit;
}

/*===============Edit profile===========*/

add_filter("wcra_editProfile_callback" , "wcra_editProfile_callback_handler");
function wcra_editProfile_callback_handler($param)
{
	$prm = $param['userId'];
	if(isset($prm) && $prm != '')
	{
		$data['billing_first_name'] = $param['firstname'];
		$data['billing_last_name'] = $param['lastname'];
		$data['billing_email'] = $param['email'];
		$data['billing_phone'] = $param['phonenumber'];
		$data['billing_address_1'] = $param['address'];
		$data['billing_city'] = $param['city'];
		$data['billing_country'] = $param['country'];
		foreach ($data as $meta_key => $meta_value) 
		{
			update_user_meta( $prm, $meta_key, $meta_value );
		}
		$response['success'] = true;
		$response['message'] = 'Success';
	}
	else
	{
		$response['success'] = false;
		$response['message'] = 'Parameter Missing';
	}
	echo json_encode($response);exit;
}

/*=============Update profile===========*/

add_filter("wcra_updateProfileImage_callback" , "wcra_updateProfileImage_callback_handler");
function wcra_updateProfileImage_callback_handler($param)
{
	$userId = $param['userId'];
	if(isset($userId) && $userId != '')
	{
		if(isset($_FILES) && !empty($_FILES['image']['name']))
		{
			$old_attach=get_user_meta($userId, 'img_profile', true);
			if(is_numeric($old_attach))
			{
			    wp_delete_attachment($old_attach, true);
			}
			$addroot = '/wp-content/uploads/2020/01/';
		    $current_user_id = $userId;
		    $extimage = '.jpg';
		    $filename = $addroot . $_FILES['image']['name'];
		    $filetype = wp_check_filetype(basename($filename), null);
		    $newname = $addroot.strtotime(date('H:i:s')).'.'.$filetype['ext'];
		   	$wp_upload_dir = wp_upload_dir();
		    $attachment = array(
		        'guid' => $wp_upload_dir['path'] . '/' . basename($newname),
		        'post_mime_type' => $filetype['type'],
		        'post_title' => preg_replace('/\.[^.]+$/', '', basename($newname)),
		        'post_content' => '',
		        'post_status' => 'inherit'
		    );
		    $attach_id = wp_insert_attachment($attachment, $newname, 0);
		    require_once( ABSPATH . 'wp-admin/includes/image.php' );
		    $attach_data = wp_generate_attachment_metadata( $attach_id, $newname );
			wp_update_attachment_metadata( $attach_id, $attach_data );
		    update_user_meta($current_user_id, 'img_profile', $attach_id);
		    move_uploaded_file($_FILES['image']['tmp_name'],$wp_upload_dir['path'] . '/' . basename($newname));
		    $response['success'] = true;
			$response['message'] = 'Profile image Successfully updated';
		}
		else
		{
			$response['success'] = false;
			$response['message'] = 'Error while uploading image';
		}
	}
	else
	{
		$response['success'] = false;
		$response['message'] = 'Parameter Missing';
	}
	echo json_encode($response);exit;
}

/*=============User FAQ=============*/

add_filter("wcra_userFaq_callback" , "wcra_userFaq_callback_handler");
function wcra_userFaq_callback_handler($param)
{
	global $cq_options;
	$args = array(
		'posts_per_page'   => -1,
		'post_type'        => 'faq',
		'post_status'      => 'publish',
	);
	$faqs = get_posts($args);
	if(!empty($faqs))
	{
		$data['title'] = $cq_options['faq-title'];
		$data['subtitle'] = $cq_options['faq-subtitle'];
		$faqDetail = array();
		foreach ($faqs as $key => $faq) 
		{ 
			$faqDetail[$key]['title'] = $faq->post_title;
			$faqDetail[$key]['content'] = $faq->post_content;
		}
		$data['detail'] = $faqDetail;
	}
	$response['success'] = true;
	$response['faq_detail'] = $data;
	echo json_encode($response);exit;
}

/*=============User Bookings=============*/

add_filter("wcra_userBookings_callback" , "wcra_userBookings_callback_handler");
function wcra_userBookings_callback_handler($param)
{
	$userId = $param['userId'];
	$data = array();
	if(isset($userId) && $userId != '')
	{
		$type = $param['type'];
		if(isset($type) && $type != '')
		{
			$bookings = get_user_booking( $userId , $type, $type );
			if(!empty($bookings))
			{
				global $cq_options;
				foreach ( $bookings as $key=>$booking )
				{
					$data[$key]['check_in_month'] = $booking->get_start_date( 'M' );
					$data[$key]['check_in_date'] = $booking->get_start_date( 'd' );
					$data[$key]['check_out_month'] = $booking->get_end_date( 'M' );
					$data[$key]['check_out_date'] = $booking->get_end_date( 'd' );
					$data[$key]['destination'] = "";
					$data[$key]['booking_order'] = "";
					if ( $booking->get_product() )
					{
						$data[$key]['destination'] = $booking->get_product()->get_title();
					}
					if ( $booking->get_order() ) 
					{
						$data[$key]['booking_order'] = '#'.$booking->get_order()->get_order_number(); 
					}
					$data[$key]['status'] = $booking->get_status();
					$data[$key]['payment'] = $booking->get_order()->get_status();
					$data[$key]['important_note'] = $cq_options['notice-extra-data'];
				}
			}
			$response['success'] = "true";
			$response['message'] = 'Success';
			$response['detail'] = $data;
		}
		else
		{
			$response['success'] = "false";
			$response['message'] = 'Parameter Missing';
			$response['detail'] = $data;
		}
	}
	else
	{
		$response['success'] = "false";
		$response['message'] = 'Parameter Missing';
	}
	echo json_encode($response);exit;
}



//
// Your code goes below
//

add_option('property_booking_data');
add_action('admin_init','cinqueterre_init');
function cinqueterre_init()
	{
	// Image size for single posts
	add_image_size( 'single-lux-thumbnail', 1024, 687 );
	
	add_meta_box('cinqueterre_property', __('Property ID','cinqueterrerivierra'), 'cinqueterre_meta_property', 'product', 'side', 'low');
	add_meta_box('cinqueterre_property_citra', __('CITRA CODE','cinqueterrerivierra'), 'cinqueterre_meta_citracode', 'product', 'side', 'low');
	add_meta_box('cinqueterre_luxury', __('Luxury Status','cinqueterrerivierra'), 'cinqueterre_meta_luxury', 'product', 'side', 'low');
	add_meta_box('cinqueterre_instant', __('Instant Booking','cinqueterrerivierra'), 'cinqueterre_meta_instant', 'product', 'side', 'low');
	add_meta_box('cinqueterre_season_terrif', __('Season Tariff & Header Image','cinqueterrerivierra'), 'cinqueterre_meta_season_terrif', 'product', 'normal', 'high');
	add_action('save_post','cinqueterre_meta_save');
	}

function cinqueterre_meta_season_terrif() {
	global $post;
	$season_duration = sanitize_text_field( get_post_meta( get_the_ID(), 'season_duration', true ) ); 
	$season_amount   = sanitize_text_field( get_post_meta( get_the_ID(), 'season_amount', true ) ); 
	$min_stay        = sanitize_text_field( get_post_meta( get_the_ID(), 'min_stay', true ) );
	$header_img      = sanitize_text_field( get_post_meta( get_the_ID(), 'header_img', true ) );
	$cleaning_fee    = sanitize_text_field( get_post_meta( get_the_ID(), 'cleaning_fee', true ) );
	$city_tax        = sanitize_text_field( get_post_meta( get_the_ID(), 'city_tax', true ) );

	$season_duration_1 = unserialize( $season_duration );
	$season_amount_1   = unserialize( $season_amount );
	$min_stay_1        = unserialize( $min_stay );
	?>	<style type="text/css">
		.wl-ext-fields {
		    padding: 10px;
		    border: 1px solid #ccc;
		    margin-bottom: 8px;
		    max-width: 203px;
		    display: inline-block;
		}
		a.btn.btn-danger.btn-sm.remove_field {
		    color: white;
		    background: red;
		    text-decoration: none;
		    padding: 4px;
		    margin-top: 5px;
		    border-radius: 4px;
		    box-shadow: 1px 1px 2px 1px #ccc;
		}
		.form-group.row {
		    margin-bottom: 7px;
		}
		button.btn.btn-success.add_field_button.add_field_button-season {
		    background: #149f14;
		    color: #fff;
		    border: none;
		    padding: 6px;
		    border-radius: 6px;
		    box-shadow: 2px 2px 3px 0px #ccc;
		}
	</style>
	<h4>Season Tariff</h4>
	<div class='input_fields_wrap_season'>
		<?php if ( isset( $season_duration_1 ) && ! empty ( $season_duration_1 ) ) { ?>
		<?php $size = sizeof( $season_duration_1 );
			for ( $i=0; $i < $size; $i++ ) { 
			?>
				<div class="wl-ext-fields">
					<div class="form-group row">
						<label for="title" class="col-3 col-form-label wl-txt-label">Season Duration</label>
						<div class="col-9">
							<input type="text" class="form-control" id="season_duration" name="season_duration[]" placeholder="Enter title" value="<?php if( ! empty( $season_duration_1[$i] ) ) { echo $season_duration_1[$i]; } ?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="$id-desc" class="col-3 col-form-label wl-txt-label">Amount</label>
						<div class="col-9">
							<input type="text" class="form-control" id="season_amount" name="season_amount[]" placeholder="Enter title" value="<?php if( ! empty( $season_amount_1[$i] ) ) { echo $season_amount_1[$i]; } ?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="title" class="col-3 col-form-label wl-txt-label">Min Stay</label>
						<div class="col-9">
							<input type="text" class="form-control" id="min_stay" name="min_stay[]" placeholder="Enter title" value="<?php if( ! empty( $min_stay_1[$i] ) ) { echo $min_stay_1[$i]; } ?>"></div>
						</div>
						<a href="#" class="btn btn-danger btn-sm remove_field">Remove</a>
				</div>
			<?php
			} }
		?>	
	</div>
  <button class='btn btn-success add_field_button add_field_button-season'>Add More</button></br></br>

  <script type="text/javascript">
		jQuery(document).ready(function() {
		    var max_fields = 40;
		    var wrapper    = jQuery(".input_fields_wrap_season");
		    var add_button = jQuery(".add_field_button-season");

		    var x = 0;
		    jQuery(add_button).click(function(e){
		        e.preventDefault();

		        if ( jQuery(".wl-ext-fields").parents("#input_fields_wrap_season").length == 1 ) {
		        	var count = jQuery("#input_fields_wrap_season").children().length;
		        	x = count;
		        }

		        if(x < max_fields){
		            x++;
		            jQuery(wrapper).append('<div class="wl-ext-fields"><div class="form-group row"><label for="title" class="col-3 col-form-label wl-txt-label">Season Duration</label><div class="col-9"><input type="text" class="form-control" id="season_duration" name="season_duration[]" placeholder="Enter title"></div></div><div class="form-group row"><label for="$id-desc" class="col-3 col-form-label wl-txt-label">Amount</label><div class="col-9"><input type="text" class="form-control" id="season_amount" name="season_amount[]" placeholder="Enter title"></div></div><div class="form-group row"><label for="title" class="col-3 col-form-label wl-txt-label">Min Stay</label><div class="col-9"><input type="text" class="form-control" id="min_stay" name="min_stay[]" placeholder="Enter title"></div></div><a href="#" class="btn btn-danger btn-sm remove_field">Remove</a></div>');
		        }
		    });

		    jQuery(wrapper).on("click",".remove_field", function(e){
		        e.preventDefault(); jQuery(this).parent('div').remove(); x--;
		    });
		});
	</script>

	<div class="form-group row">
        <label for="header_img" class="col-3 col-form-label wl_custom_in"><b><?php _e( 'Header Image', 'cinqueterreriviera' ); ?></b></label>
        <div class="col-7" style="display: inline-block;">
            <input type="text" placeholder="<?php _e( "Image URL", "selements" ); ?>" name="header_img" id="header_img" class="form-control" value="<?php if ( ! empty( $header_img ) ) {
                echo esc_attr( $header_img );
            } ?>">
        </div>
        <div class="col-2" style="display: inline-block;">
            <input type="button" name="upload-btn" id="upload_featured_img" class="button-secondary button" value="<?php _e( 'Upload Image', 'cinqueterreriviera' ); ?>">
        </div>
    </div>
    <?php if ( ! empty( $header_img ) ) { ?>
    	<div id="img_featured_div"></div>
        <img id="featured_img_tag" src="<?php  echo esc_attr( $header_img ); ?>" style ="width: 200px;display:block;">
    <?php } ?>

    <div class="form-group row">
        <label for="cleaning_fee" class="col-3 col-form-label wl_custom_in"><b><?php _e( 'Cleaning Fee Amount', 'cinqueterreriviera' ); ?></b></label>
        <div class="col-9" style="display: inline-block;">
            <input type="number" placeholder="Price" name="cleaning_fee" id="cleaning_fee" class="form-control " value="<?php if ( ! empty( $cleaning_fee ) ) {
                echo esc_attr( $cleaning_fee );
            } ?>">
        </div>
    </div>
    <div class="form-group row">
        <label for="city_tax" class="col-3 col-form-label wl_custom_in"><b><?php _e( 'City Tax Amount', 'cinqueterreriviera' ); ?></b></label>
        <div class="col-9" style="display: inline-block;">
            <input type="text" placeholder="Price" name="city_tax" id="city_tax" class="form-control " value="<?php if ( ! empty( $city_tax ) ) {
                echo esc_attr( $city_tax );
            } ?>">
        </div>
    </div>
    <style type="text/css">
    	label.col-3.col-form-label.wl_custom_in {
		    font-size: 15px;
		    margin-right: 3%;
		}
		input#header_img, #cleaning_fee, #city_tax {
		    width: 599px;
		    border-radius: 11px;
		    padding: 10px;
		    height: auto;
		}
		input#upload_featured_img {
		    border-radius: 10px;
		    padding: 6px 14px;
		    height: auto;
		    position: relative;
		    top: -11px;
		}
	</style>
    <script type="text/javascript">
    	/* Marker image Upload */
		jQuery(document).ready(function () {
		    jQuery('#upload_featured_img').click(function (e) {
		        e.preventDefault();
		        var button = this;
		        var image = wp.media({
		            title: 'Upload Image',
		            multiple: false
		        }).open()
		            .on('select', function (e) {
		                var uploaded_image = image.state().get('selection').first();
		                var old_image      = jQuery('#header_img').val();
		                var location_marker_img = uploaded_image.toJSON().url;

		                if ( old_image.lenght !== 0 ){
	                      jQuery('#upload_featured_img').siblings('input').val('');
	                    }

		                jQuery('#header_img').val(location_marker_img);
		                var img_tag = jQuery('#header_img').val(location_marker_img);
		                jQuery('#featured_img_tag').remove();
                    	jQuery( '#img_featured_div').prepend('<img id="id="featured_img_tag"" class="wl-upload-img-tag" src="'+location_marker_img+'" style ="width: 200px;display:block;" />');
		            });
		    });
		});
    </script>

	<?php
}

function cinqueterre_meta_property(){
	global $post;
	$propert_id = sanitize_text_field( get_post_meta( get_the_ID(), 'propert_id', true )); ?>
	<p><input  name="propert_id" id="propert_id"  placeholder="<?php _e('Property ID','cinqueterrerivierra');?>" type="text" value="<?php if (!empty($propert_id)) echo esc_attr($propert_id); ?>"><br><br>
	<span class="howto"><?php _e('Please Put Property ID which is created in Rental United property listing. When you create property there.','cinqueterrerivierra');?></span>
	</p>
<?php }

function cinqueterre_meta_citracode(){
	global $post;
	$citra_code = sanitize_text_field( get_post_meta( get_the_ID(), 'citra_code', true )); ?>
	<p><input  name="citra_code" id="citra_code"  placeholder="<?php _e('CITRA CODE','cinqueterrerivierra');?>" type="text" value="<?php if (!empty($citra_code)) echo esc_attr($citra_code); ?>"><br><br>
	<span class="howto"><?php _e('Please Put CITRA Code here.','cinqueterrerivierra');?></span>
	</p>
<?php }

function cinqueterre_meta_luxury(){
	global $post;
	$luxury_st = sanitize_text_field( get_post_meta( get_the_ID(), 'luxury_st', true )); ?>
	<style>
	.custom_checkbox input[type=checkbox]{
	  height: 0;
	  width: 0;
	  visibility: hidden;
	}
	
	.custom_checkbox label {
	    cursor: pointer;
	    text-indent: -9999px;
	    width: 55px;
	    height: 26px;
	    background: #32373c;
	    display: inline-block;
	    border-radius: 100px;
	    position: relative;
	}
	
	.custom_checkbox label:after {
	    content: '';
	    position: absolute;
	    top: 5px;
	    left: 5px;
	    width: 18px;
	    height: 17px;
	    background: #f1f1f1;
	    border-radius: 90px;
	    transition: 0.3s;
	}
	
	.custom_checkbox input:checked + label {
	    background: #36c94f;
	}
	
	.custom_checkbox input:checked + label:after {
	  left: calc(100% - 5px);
	  transform: translateX(-100%);
	}
	
	.custom_checkbox label:active:after {
	    width: 17px;
	}
	</style>
	<div class="custom_checkbox">
	<p><input type="checkbox" name="luxury_st" id="luxury_st" <?php if(!empty($luxury_st)) { echo 'checked'; } ?>/><label for="luxury_st">Toggle</label>&nbsp;&nbsp;&nbsp;Add this to Luxury Property</p>
	<span class="howto"><?php _e('Check it if you wanna make this "Luxury Property"','cinqueterrerivierra');?></span>
	</div>

<?php }

function cinqueterre_meta_instant(){
	global $post;
	$instant_book = sanitize_text_field( get_post_meta( get_the_ID(), 'instant_book', true )); ?>
	<style>
	.custom_checkbox input[type=checkbox]{
	  height: 0;
	  width: 0;
	  visibility: hidden;
	}
	
	.custom_checkbox label {
	    cursor: pointer;
	    text-indent: -9999px;
	    width: 55px;
	    height: 26px;
	    background: #32373c;
	    display: inline-block;
	    border-radius: 100px;
	    position: relative;
	}
	
	.custom_checkbox label:after {
	    content: '';
	    position: absolute;
	    top: 5px;
	    left: 5px;
	    width: 18px;
	    height: 17px;
	    background: #f1f1f1;
	    border-radius: 90px;
	    transition: 0.3s;
	}
	
	.custom_checkbox input:checked + label {
	    background: #36c94f;
	}
	
	.custom_checkbox input:checked + label:after {
	  left: calc(100% - 5px);
	  transform: translateX(-100%);
	}
	
	.custom_checkbox label:active:after {
	    width: 17px;
	}
	</style>
	<div class="custom_checkbox">
	<p><input type="checkbox" name="instant_book" id="instant_book" <?php if(!empty($instant_book)) { echo 'checked'; } ?>/><label for="instant_book">Toggle</label>&nbsp;&nbsp;&nbsp;Enable for instant booking</p>
	<span class="howto"><?php _e('Check it if you wanna make this "Luxury Property"','cinqueterrerivierra');?></span>
	</div>

<?php }

function cinqueterre_meta_save($post_id) 
{	
	if((defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || (defined('DOING_AJAX') && DOING_AJAX) || isset($_REQUEST['bulk_edit']))
		return;
		
	if ( ! current_user_can( 'edit_page', $post_id ) )
	{     return ;	} 		
	if(isset($_POST['post_ID']))
	{ 	
		$post_ID = $_POST['post_ID'];				
		$post_type=get_post_type($post_ID);
		
		if($post_type=='product') {	
			update_post_meta($post_ID, 'citra_code', sanitize_text_field($_POST['citra_code']));
			update_post_meta($post_ID, 'propert_id', sanitize_text_field($_POST['propert_id']));
			update_post_meta($post_ID, 'luxury_st', sanitize_text_field($_POST['luxury_st']));
			update_post_meta($post_ID, 'instant_book', sanitize_text_field($_POST['instant_book']));
			update_post_meta($post_ID, 'header_img', sanitize_text_field($_POST['header_img']));
			update_post_meta($post_ID, 'cleaning_fee', sanitize_text_field($_POST['cleaning_fee']));
			update_post_meta($post_ID, 'city_tax', sanitize_text_field($_POST['city_tax']));

			if ( isset( $_POST['season_duration'] ) && ! empty ( $_POST['season_duration'] ) ) {
				$season_duration = serialize($_POST['season_duration']);
			}
			if ( isset( $_POST['season_amount'] ) && ! empty ( $_POST['season_amount'] ) ) {
				$season_amount = serialize($_POST['season_amount']);
			}
			if ( isset( $_POST['min_stay'] ) && ! empty ( $_POST['min_stay'] ) ) {
				$min_stay = serialize($_POST['min_stay']);
			}
			update_post_meta( $post_ID, 'season_duration', sanitize_text_field( $season_duration ) );
			update_post_meta( $post_ID, 'season_amount', sanitize_text_field( $season_amount ) );
			update_post_meta( $post_ID, 'min_stay', sanitize_text_field( $min_stay ) );
		}
	}			
} 

add_action( 'wp_head', 'theme_enqueue_sdk' );
function theme_enqueue_sdk()
{
	if(!is_home() && !is_front_page()){
		wp_enqueue_script( 'masterslider-min-js', plugins_url() . '/masterslider/public/assets/js/masterslider.min.js' );
	}
}

add_action('admin_menu' , 'webliar_admin_menu_Menu_Function');
function webliar_admin_menu_Menu_Function() {
    add_submenu_page('themes.php', 'Property Order', 'Property Order', 'administrator', 'Property-order', 'weblizar_auth_setting_Page_Function');
}
function weblizar_auth_setting_Page_Function()
{
    wp_enqueue_script('jquery');
    //wp_enqueue_style('bootstrap_css', get_template_directory_uri(). '/css/bootstrap.css');
    //wp_enqueue_script('bootstrap_js', get_template_directory_uri(). '/js/bootstrap.min.js');
    //Setting-page
    require_once("port_setting.php");
}

add_action( 'admin_enqueue_scripts', 'wpm_ajax_enqueue_scripts' );
function wpm_ajax_enqueue_scripts() {
    // Note that the first parameter of wp_enqueue_script() matches that of wp_localize_script.
    wp_enqueue_script( 'featured_ajax', get_stylesheet_directory_uri(). '/js/featured.js', array('jquery') );
    // The second parameter ('aj_ajax_url') will be used in the javascript code.
    wp_localize_script( 'featured_ajax', 'ajax_featured', array(
                        'ajax_url' => admin_url( 'admin-ajax.php' ),
                        'featured_nonce' => wp_create_nonce('featured_ajax_nonce') 
    ));
    wp_enqueue_media();
}

// action call for featurd.js
add_action( 'wp_ajax_nopriv_wpm_ajax_request', 'wpm_ajax_featured_process' );
add_action( 'wp_ajax_wpm_ajax_request', 'wpm_ajax_featured_process' );  // For logged in users.
function wpm_ajax_featured_process() {
    check_ajax_referer( 'featured_ajax_nonce', 'nounce' );  // This function will die if nonce is not correct.
    
    if(isset($_POST['pid_array']) && isset($_POST['cat_name'])){
    	add_option("short_option_cp");
        update_option($_POST['cat_name'].'short_option_cp', $_POST);
    }
    wp_die();
}

require_once( 'assets/inc/inc.php' );
require_once( 'assets/inc/rental_class.php' );
require_once( 'assets/inc/mdia_class.php' );
require_once ( 'assets/menu/default-menu-walker.php' );
require_once ( 'assets/menu/wl-nav-walker.php' );
require_once ( 'vendor/autoload.php' );
require_once ( 'stripe/vendor/autoload.php' );
require_once ( 'assets/inc/tickets-booking.php' );
require_once ( 'assets/inc/promo-banner-widget.php' );
require_once ( 'new-assets/inc/new-inc.php' );
require_once ( 'updateList.php' );


function cp_load_custom_search_template(){
    if ( isset( $_REQUEST['search'] ) == 'advanced' && isset( $_REQUEST['new_search_layout'] ) == 'new_search_layout' ) {
        require('property_search1.php');
        die();
    } elseif ( isset( $_REQUEST['search'] ) == 'advanced' ) {
        require('property_search.php');
        die();
    }
} 
add_action('init','cp_load_custom_search_template');

function admin_bar(){

  if(is_user_logged_in()){
    add_filter( 'show_admin_bar', '__return_true' , 1000 );
  }
}
add_action('init', 'admin_bar' );

add_action( 'wp_clear_cache_cron_task_hook', 'wp_clear_cache_cron_task' );
function wp_clear_cache_cron_task() {
  // Folder path to be flushed 
  $folder_path = get_stylesheet_directory() . "/cache-files"; 
     
  // List of name of files inside 
  // specified folder 
  $files = glob($folder_path.'/*');  
     
  // Deleting all the files in the list 
  foreach( $files as $file ) { 
     
      if ( is_file( $file ) )   
        // Delete the given file 
        unlink( $file );  
  } 
}

function register_my_menu_home_3() {
  register_nav_menu('new-menu',__( 'New Menu' ));
}
add_action( 'init', 'register_my_menu_home_3' );

function new_search_page( $template ) {    
  global $wp_query;   
  $post_type = get_query_var('post_type');   
  if( $wp_query->is_search && $post_type == 'products' ) {
    return locate_template('archive-search.php');  //  redirect to archive-search.php
  }   
  return $template;   
}
add_filter('template_include', 'new_search_page');

add_option('guest_info_data');
// function cp_qform2_save_form_data($form) {
//     $values = $form->getValues();
//     $guest_info_data = get_option( 'guest_info_data' );

//     if ( empty ( $guest_info_data ) ) {
//     	$guest_info_data = array();
//     }
//     array_push( $guest_info_data, $values );

//     update_option( 'guest_info_data', $guest_info_data );

// }
// add_action('iphorm_post_process_3', 'cp_qform2_save_form_data');

// function cp_qform2_save_form_data_session($form)
// {
//     $_SESSION['iphorm_3'] = $form;
// }
// add_action('iphorm_post_process_3', 'cp_qform2_save_form_data_session');

add_filter('quform_post_process_3', function (array $result, $form) {
    $values = $form->getValues();
    $guest_info_data = get_option( 'guest_info_data' );

    if ( empty ( $guest_info_data ) ) {
    	$guest_info_data = array();
    }
    array_push( $guest_info_data, $values );

    update_option( 'guest_info_data', $guest_info_data );
 
    return $result;
}, 10, 2);


if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'New Theme Settings',
		'menu_title'	=> 'New Theme Settings',
		'menu_slug' 	=> 'new-theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	// acf_add_options_sub_page(array(
	// 	'page_title' 	=> 'Theme Header Settings',
	// 	'menu_title'	=> 'Header',
	// 	'parent_slug'	=> 'theme-general-settings',
	// ));
	
	// acf_add_options_sub_page(array(
	// 	'page_title' 	=> 'Theme Footer Settings',
	// 	'menu_title'	=> 'Footer',
	// 	'parent_slug'	=> 'theme-general-settings',
	// ));
	
}